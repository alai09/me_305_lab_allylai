'''!@file       taskUser.py
    @brief      A generator to implement UI tasks as part of an FSM.
    @details    During each iteration the generator initiates actions based 
                on the entered sampling period and user inputs. The taskUser interacts
                with the taskMotor and taskEncoder classes to translate user input to
                interaction with hardware. 
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP
import array

def taskUserFcn(taskName, period, motor_num, z_Flag, position, delta, m_Flag, duty_cycle, fault):
    '''!@brief              A generator function which returns a value for state related to a UI task
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param z_Flag       A Share object which is used to indicate that an 
                            action related to entering the "z" key should be performed
        @param position     A Share object respresenting the position attribute of the encoder
        @param delta        A Share object respresenting the delta attribute of the encoder
    '''
    
    ## @brief The state that the UI is currently in.
    state = 0
    
    ## @brief A counter variable used for data collection. 
    count = 0 
    
    ## @brief Variable for time used in data collection
    time = 0.00
    
    ## @brief An array for data used in data collection
    DatArray = array.array('l',[])
    
    ## @brief An array for time data used in data collection
    timeArray = array.array('f', [])
    
    ## @brief A boolean used to indicate whether the duty cycle is being set or data is being collected
    collecting = False
    ## @brief A number used to keep track of the sum of all the velocities sampled to calculate average velocity
    velo_sum = 0
    ## @brief An integer used to keep track of how many velocities have been sampled to calculate average velocity
    v_count = 0
    ## @brief An array for duty cycle specified data collection
    DutyArray = array.array('l',[])
    ## @brief An array for average velocity data used in data collection
    VelocityArray = array.array('f', [])
 
    ## @brief An integer representing the number of ticks per revolution specified for the encoder
    CPR = 4000   
 
    ## @brief The time that the UI tasks started. 
    start_time = ticks_us()
    
    ## @brief The start time plus the period indicating when the next cycle of UI begins
    next_time = ticks_add(start_time, period)
    
    ## @brief A (virtual) serial port object used for getting characters cooperatively.
    serport = USB_VCP()
    
    print('+-------------------------------------+')
    print('| Welcome to the Motor Interface      |')
    print('|-------------------------------------|')
    print('| To use the interface, press:        |')
    print('| "Z" to zero the position            |')
    print('| "P" to get position                 |')
    print('| "D" to get the delta value          |')
    print('| "V" to get velocity                 |')
    print('| "G" to collect data for 30 seconds  |')
    print('| "H" to print this help menu         |')
    print('| "m" to enter duty cycle for motor 1 |')
    print('| "M" to enter duty cycle for motor 2 |')
    print('| "C" to clear fault conditions       |')
    print('| "T" to start testing interface      |')    
    print('| "S" to stop data collection or test |')
    print('|      interface prematurely          |')
    print('+-------------------------------------+')    
      
    while True:
        ## @brief The current time.
        current_time = ticks_us()
   
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            
            if state == 0:
          
                state = 1  
   
            elif state == 1:
                
                if fault.read() == True:
                    state = 6
                elif serport.any():
                    ## @brief The character input for UI.
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'z','Z'}:
                        print('Typed Z for Zero')
                        state = 2
                        z_Flag.write(True)
                    elif charIn in {'p','P'}:
                        print('Typed P for Position')
                        print(position.read())
                    elif charIn in {'v','V'}:
                        print('Typed V for Velocity')
                        delta1 = float(delta.read())/CPR
                        velocity = delta1/0.01
                        print(velocity)
                    elif charIn in {'d','D'}:
                        print('Typed D for Delta')
                        print(delta.read())
                    elif charIn in {'c','C'}:
                        fault.write(False)
                    elif charIn in {'g','G'}:
                        state = 3
                        print('Data Collection Beginning')
                    elif charIn in {'s','S'}:
                            print('Data is not being collected, press G to start data collection')
                    elif charIn in {'h','H'}:
                        print('+------------------------------------+')
                        print('| Welcome to the Motor Interface     |')
                        print('|------------------------------------|')
                        print('| To use the interface, press:       |')
                        print('| "Z" to zero the position           |')
                        print('| "P" to get position                |')
                        print('| "D" to get the delta value         |')
                        print('| "V" to get velocity                |')
                        print('| "G" to collect data for 30 seconds |')
                        print('| "H" to print this help menu        |')
                        print('| "m" to enter duty cycle for motor 1|')
                        print('| "M" to enter duty cycle for motor 2|')
                        print('| "C" to clear fault conditions      |')
                        print('| "T" to start testing interface     |')    
                        print('| "S" to stop data collection or test|')
                        print('|      interface prematurely         |')
                        print('+------------------------------------+')  
                    elif charIn == 'm' or charIn == 'M':
                        print(charIn)
                        if charIn == 'm':
                            motor_num.write(1)
                        else:
                            motor_num.write(2)
                        state = 5
                    elif charIn == 't' or charIn == 'T':
                        state = 7
                        collecting = False
                        motor_num.write(1)
                        print("Entering Testing Interface...")
                        print(" ")
                        print('Please enter a duty cycle between -100 and 100 for Motor 1')
                        
                    
            # for input z zero encoder position
            elif state == 2:
                
                if z_Flag.read() == False:
                    state = 1 # transition to state 0
                    
            # collect encoder data        
            elif state == 3:
                
                if count <= 3001:
                    
                    DatArray.append(position.read())
                    
                    timeArray.append(time)
                    
                    time += (1/100)
                    
                    count += 1
                    
                    if serport.any() == True:
                        if serport.read(1).decode() in {'s','S'}:
                            state = 4
                            
                else:
                    state = 4
                    
            # print recorded encoder data        
            elif state == 4:
                print("Data Collection Stopped")
                print('----------Start Data----------')
                for i,n in zip(DatArray, timeArray):
                    print(str(i) + ', ' + "{:.2f}".format(n))
                print('----------End Data----------')    
                DatArray = array.array('l',[])
                timeArray = array.array('f', [])
                count = 0
                time = 0
                state = 1
                
            # change motor duty cycle
            elif state == 5:
                print('Please enter a duty cycle between -100 and 100')
                buffer = ''
                while True:
                    if serport.any():
                        Char = serport.read(1).decode()
                        if Char.isdigit():
                            buffer += Char
                        if (Char == '-') and (len(buffer) == 0):
                            buffer += Char
                        if (Char == '\b') or (Char == '\x08'):
                            if len(buffer) != 0:
                                buffer(-1).remove
                        if Char == '\r':
                            buffer = str(buffer) 
                            try:
                                if (int(buffer) > 100) or (int(buffer) < -100):
                                    print('Duty cycle out of range')
                                    buffer = ''
                                    pass
                                else:
                                    duty_cycle.write(int(buffer))
                                    m_Flag.write(True)
                                    print(f'Duty Cycle of Motor {motor_num.read()} Set to {buffer}')
                                    break
                            except:
                                print('Invalid Input for Duty Cycle')
                    if fault.read() == True:
                        state = 6
                        break
                    if m_Flag.read() == False:
                        state = 1
            
            # clear a fault condition from the motor and enable the motor driver
            elif state == 6:
                print('Motor Fault Detected')
                print('Press "c" to clear the fault and re-enable the motor driver:')
                while True:
                    if serport.any():
                        Char = serport.read(1).decode()
                        if Char == 'c' or Char == 'C':
                            print("Fault Cleared")
                            fault.write(False)
                            state = 1
                            break
                
            # testing interface to try different duty cycles for motor 1
            elif state == 7:
                buffer = ''
                if fault.read() == True:
                    state = 6
                    break
                if collecting is True:
                    if count < 1001:
                        if count%50 == 0:
                            ## @brief The current velocity measured by the encoder
                            velo = (float(delta.read())/CPR)/0.01
                            if count > 500:      
                                velo_sum += velo
                                v_count += 1
                        count += 1     
                    else:
                        VelocityArray.append(velo_sum/v_count)
                        DutyArray.append(duty_cycle.read())
                        print("~ Finished Collecting Velocity Data ~")
                        collecting = False
                        v_count = 0
                        velo_sum = 0
                        count = 0
                        print('')
                        print('Please enter a duty cycle between -100 and 100 for Motor 1 or press "s" to stop testing and print results')
                while collecting is False:
                    if serport.any():
                        Char = serport.read(1).decode()
                        if Char in {'s','S'}:
                            # Go to print state and stop motor rotation after test stops
                            state =  8
                            duty_cycle.write(0)
                            m_Flag.write(True)
                            break
                        if Char.isdigit():
                            buffer += Char
                        if (Char == '-') and (len(buffer) == 0):
                            buffer += Char
                        if (Char == '\b') or (Char == '\x08'):
                            if len(buffer) != 0:
                                buffer(-1).remove
                        if Char == '\r':
                            buffer = str(buffer) 
                            try:
                                if (int(buffer) > 100) or (int(buffer) < -100):
                                    print('Duty cycle out of range')
                                    buffer = ''
                                    pass
                                else:
                                    duty_cycle.write(int(buffer))
                                    m_Flag.write(True)
                                    collecting = True
                                    print(f'Duty Cycle of Motor {motor_num.read()} Set to {buffer}')
                                    print("Collecting Velocity Data...")
                                    break
                            except:
                                print('Invalid Input for Duty Cycle')
                
                
                
            # print testing data (table of duty cycles and velocities)
            elif state == 8:
                print('Printing Test Data for Motor 1')
                print('')
                print('|  Duty Cycle   |   Average Velocity   |')
                print('-----------------------------------------') 
                for i,n in zip(DutyArray, VelocityArray):
                    print('      '+ str(i) + '                ' + "{:.2f}".format(n))
                print('-----------------------------------------')    
                DutyArray = array.array('l',[position.read()])
                VelocityArray = array.array('f', [0])
                count = 0
                time = 0
                state = 1
            
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield state
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None
