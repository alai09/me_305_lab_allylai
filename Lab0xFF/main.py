'''!@file       main.py
    @brief      The driving class behind implimentation of IU.
    @details    This file creates User, Encoder and Motor tasks, each that run their 
                own seperate UI's that work together to accomplish motor rotation. 
'''                           
    # @image      html Lab0x03_taskdiagram.JPG "" width=1000
    #             The Task Diagram for the taskUser, taskMotor and taskEncoder files.

import shares, taskUser, taskMotor, taskController, taskBNO055, taskTouchPanel
    

##Closed Loop Controller Shares

## @brief A value indicating the desired angular position of the platform for each closed-loop controller
ref_theta = shares.Share(0)
## @brief A value indicating the measured angular position of the platform from an IMU
meas_theta = shares.Share(0)
## @brief A value indicating the desired angular velocity of the platform for each closed-loop controller
ref_thetadot = shares.Share(0)
## @brief A value indicating the measured angular velocity of the platform from an IMU
meas_thetadot = shares.Share(0)
## @brief A tuple of values for inner and outer loop proportional controller gains (respectively)
kp = shares.Share([7.55,.185]) # 
## @brief A tuple of values for inner and outer loop derivative controller gains (respectively)
kd = shares.Share([0.33, 0.155])
## @brief A tuple of values for inner and outer loop integral controller gains (respectively)
ki = shares.Share([0, 0])
## @brief A flag that indicates whether the user has enabled closed loop control of the motors
w_Flag = shares.Share(False)
## @brief A flag that indicates that the user wants to collect touchpanel and duty cycle data
collect_Flag = shares.Share(False)
## @brief A flag indicating that the user has pressed the 's' key to end an action
s_Flag = shares.Share(False)


#Motor Control Shares

## @brief A shared variable indicating the motor to interact with
motor_num = shares.Share(0)
## @brief A shared variable indicating that the motor duty cycle should be changed
m_Flag = shares.Share(False)
## @brief A shared variable indicating the duty cycle to motor 1
duty_cycle1 = shares.Share(0)
## @brief A shared variable indicating the duty cycle to motor 2
duty_cycle2 = shares.Share(0)


#IMU Measurement and Calibration Shares

## @brief The Euler angles returned from the IMU chip
eulerangles = shares.Share([0,0,0,0,0,0])
## @brief A flag indicating whether euler angles of the platform are requested to be recorded
euler_call = shares.Share(False)
## @brief The values for angular velocity returned from the IMU
ang_vel = shares.Share([0,0,0,0,0,0])
## @brief A flag indicating whether angular velocities of the platform are requested to be recorded
ang_call = shares.Share(False)
## @brief A flag which indicates that calibration of the IMU is desired
cal_call = shares.Share(False)
## @brief A flag indicating whether angular velocities of the platform are requested to be recorded
coef_call = shares.Share(False)
## @brief A flag which idicates whether IMU coefficients are to be written to an IMU
set_call = shares.Share(False)

#Tocuh Panel Shares

## @brief A share used to store the beta matrix which converts ADC readings of the touchpanel coordinates
beta = shares.Share(False)
## @brief A flag which indicates that calibration of the touch panel is requested
touch_cal = shares.Share(False)
## @brief The x coordinate of the point of contact
x_pos = shares.Share(0)
## @brief The y coordinate of the point of contact
y_pos = shares.Share(0)
## @brief A flag which indicates whether the touch panel is currently calibrated
touch_has_cal = shares.Share(False)
## @brief A share which indicates whether to turn on alpha-beta filtering of touch panel readings
f_Flag = shares.Share(False)
## @brief The ball x and y coordinates based on readings from the touchpanel
ball_coords = shares.Share([0,0,False])
## @brief The balls velocity based on readings from the touchpanel
ball_vel = shares.Share([0,0])

# Generator Parameters

## @brief The period set for running the User, BNO055, Controller, and Motor Tasks
period = 10_000
## @brief The period set for running the Touch Panel Task
tp_period = 2_000

if __name__ == "__main__":
    '''!@brief              Constructs an empty queue of shared values
    '''
    ## @brief A list of tasks which includes one object each of the User, Encoder, and Motor Tasks
    taskList = [taskUser.taskUserFcn      ('Task User', period, motor_num, eulerangles, euler_call, ang_vel, ang_call, cal_call, m_Flag, duty_cycle1, duty_cycle2, w_Flag, ref_theta, meas_theta, kp, kd, coef_call, set_call, ki , beta, touch_cal, touch_has_cal, f_Flag, collect_Flag, s_Flag),
                taskBNO055.taskBNO055Fcn  ('Task BNO055', period, eulerangles, euler_call, ang_vel, ang_call, cal_call, coef_call, set_call),
                taskTouchPanel.taskTouchPanelFcn ('Task Touch Panel', tp_period, beta, touch_cal, touch_has_cal, f_Flag, ball_vel, ball_coords, collect_Flag),
                taskController.taskControllerFcn('Task Controller', period, m_Flag, eulerangles, ang_vel, kp, kd, duty_cycle1, duty_cycle2, w_Flag, ki, ball_vel, ball_coords, collect_Flag, s_Flag),
                taskMotor.taskMotorFcn    ('Task Motor', period, motor_num, m_Flag, duty_cycle1, duty_cycle2, w_Flag)]
    
    while True:
            try:            
                for task in taskList:
                    #print(task)
                    next(task)    
                    
                    
                    
            except KeyboardInterrupt:
                break
    print("Program Terminated")