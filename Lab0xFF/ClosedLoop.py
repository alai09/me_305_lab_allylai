'''!@file       ClosedLoop.py
    @brief      A PID controller
    @details    A class that represents a closed loop PID controller with attributes for proportional, derivative, and
                integral gain. The object can be used to calculate an output for the controller function based on inputs
                for measured and desired position and velocity values. The object also has methods which can be used to
                reset the gain values.
'''

class ClosedLoop:
    '''!@brief      A closed loop feedback controller used in motor movement. 
        @details    Objects of this class are given a kp, ki and kd value and are tested and retested for feedback control. 
    '''
     
    def __init__(self, kp, kd, ki):
        '''!@brief      Initializes and returns a ClosedLoop object 
            @details    Creates induvidual Closed Loop objects. Each object can be used to calculate an output for
                        closed-loop control based on the gain attibutes. A method 'run' takes in inputs for position
                        measurements and setpoints to calculate an output based on the gain values of the controller object.
            @param kp   The proportional gain value
            @param kd   The derivitive gain value
            @param ki   The integral gain value
        '''
        
        ## @brief The proportional gain of the closed-loop expression
        self.kp = kp
        
        ## @brief The derivitave gain of the closed-loop expression
        self.kd = kd
        
        ## @brief The integral gain of the closed-loop expression
        self.ki = ki
        
        ## @brief the sum of positional measurement used in the integral control within the closed-loop expression
        self.sum = 0
        
    
    def run(self, ref, meas, ref_dot, meas_dot):
        '''!@brief              Calculates the output of the closed-loop control based on the inputs for position and velocity measurements and setpoints.      
            @details            Runs a single iteration of the closed loop PID feedback controller.    
            @param ref          The reference positional value
            @param meas         The measured positional value
            @param ref_dot      The reference velocity value
            @param meas_dot     The measured velocity value
            @return             The closed-loop control output value 
        '''
        
        ##This block is used for integral control. Resets the self.sum variable
        if abs(meas) < 0.5:
            self.sum = 0
        else:
            self.sum += meas
       
        ## @brief self.output is the output value that will be re-tested in furtehr iterations
        self.output = self.kp*(ref - meas) + self.kd*(ref_dot - meas_dot) - self.ki*self.sum*(1/100)
        #print(self.sum)
#        print(f'D-Gain{self.kd*(ref_dot - meas_dot)}')
#        print(f'I-Gain{self.ki*self.sum*(1/100)}')
        
        ## Returns the Duty cycle/desired angle
        return self.output        
        
    def set_kp(self, kp_value):
        '''!@brief          Sets the proportional gain value of the closed-loop control expression 
            @param kp_value The desired value for proportional gain
        '''
        self.kp = kp_value
        pass
    
    def set_kd(self,kd_value):
        '''!@brief Sets the derivative gain value of the closed-loop control expression 
            @param kd_value The desired value for derivative gain
        '''
        self.kd = kd_value
        pass
    
    def set_ki(self,ki_value):
        '''!@brief      Sets the integral gain value of the closed-loop control expression  
            @param ki_value The desired value for integral gain
        '''
        self.ki = ki_value
        pass