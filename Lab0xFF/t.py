'''!@file       t.py
    @brief      A script used to evaluate the speed of a touch panel scan function
    @details    This file calls the touchpanel scanXYZ function 1000 times and divides
                the elapsed time by 1000 to get the average time to run the function
'''    

import touchpaneldrv, time 
from pyb import Pin

touchpanel = touchpaneldrv.TouchPanelDrv(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0)

i = 1
t0 = time.ticks_us()
while i < 1001:
    touchpanel.scanXYZ()
    i = i + 1
t1 = time.ticks_us()
total = t1-t0
print("Average time to run ScanXYZ() in us:")
print(total/1000)
print("Example Output for scanXYZ():")
print(touchpanel.scanXYZ())
