'''!@file       touchpaneldrv.py
    @brief      An class representing a resistive touch panel
    @details    This class initialized using inputs for each of its four pins
                and can be used to obtain the x and y coordinates of contact as
                well as sensing whether there is contact with the board
'''

from pyb import Pin, ADC
from micropython import const
class TouchPanelDrv: 


    def __init__ (self, pinXp, pinXm, pinYp, pinYm): 
        '''!@brief      Initializes and returns an object associated a resitive touch panel
            @details    Objects of this class are instantiated using 4 pins which are used to
                        obtain readings from the touch panel
        '''
        
        ## @brief A constant value used to indicate the mode of a pin as Push-Pull Output
        self._ppOut = const(Pin.OUT_PP)
        ## @brief A constant value used to indicate the mode of a pin as Analog
        self._alg = const(Pin.ANALOG)
        
        # Xp = left; Xm = right; Yp = top; Ym = bottom; Initially set as push-pull out
        
        ## @brief A pin object representing the Xp pin of the touch panel
        self.Xp = Pin(pinXp, self._ppOut) 
        ## @brief A pin object representing the Xm pin of the touch panel    
        self.Xm = Pin(pinXm, self._ppOut) 
        ## @brief A pin object representing the Yp pin of the touch panel
        self.Yp = Pin(pinYp, self._ppOut) 
        ## @brief A pin object representing the Ym pin of the touch panel    
        self.Ym = Pin(pinYm, self._ppOut) 
    
    def scanX(self):
        '''!@brief      Returns the scaled x value of contact in mm.
            @details    This method assigns four pins attached to a resistive touch panel to push-pull high or low or analog setting
                        in order to measure the x coordinate of the contact. The settings are Yp analog, Ym analog, Xp high, and Xm low
                        with Yp being probd for ADC output. The output is scaled using constants which assume the x
                        length of the panel is 176mm and does not account for error in the touchpanel readings.
            @return     The approximate x-coordinate of the panel contact based on estimated constant scale factors.
        '''
        self.Yp.init(self._alg) 
        ## @brief an ADC object linked to pin Yp
        adcYp = ADC(self.Yp)
        self.Ym.init(self._alg)
        self.Xp.init(self._ppOut,True)        
        self.Xm.init(self._ppOut,False)         
        return adcYp.read()*176/4096-88
    
    def scanY(self):
        '''!@brief      Returns the scaled y value of contact in mm.
            @details    This method assigns four pins attached to a resistive touch panel to push-pull high or low or analog setting
                        in order to measure the y coordinate of the contact. The settings are Xp analog, Xm analog, Yp high, and Ym low
                        with Xp being probd for ADC output.The output is scaled using constants which assume the y
                        length of the panel is 100mm and does not account for error in the touchpanel readings.
            @return     The approximate y-coordinate of the panel contact based on estimated constant scale factors.            
        '''
        self.Xp.init(self._alg) 
        ## @brief an ADC object linked to pin Xp
        adcXp = ADC(self.Yp)
        self.Xm.init(self._alg)
        self.Yp.init(self._ppOut,True)        
        self.Ym.init(self._ppOut,False)         
        return adcXp.read()*100/4096-50
        
    def scanZ(self):
        '''!@brief      Returns a value which is True is thr is contact with the touchpanel and False otherwise
            @details    This method assigns four pins attached to a resistive touch panel to push-pull high or low or analog setting
                        in order to measure a change in voltage caused by contact. The settings are Yp analog, Xm analog, Xp high, and Ym low
                        with Yp being probd for ADC output.The output is True if the ADC output is greater than zero and false if not.
            @return     The ADC output indicating contact with the touch panel                
        '''
        self.Yp.init(self._alg) 
        adcYp = ADC(self.Yp)
        self.Xm.init(self._alg)
        self.Xp.init(self._ppOut,True)        
        self.Ym.init(self._ppOut,False)         
        return adcYp.read()
    
#    def scanXYZ(self):
#        '''!@brief      Returns the approximate x and y coordinates of a touch (in millimeters) and a boolean representing contact
#            @details    This method assigns four pins attached to a resistive touch panel to push-pull high or low or analog setting
#                                in order to measure the x coordinate, y coordinate, and whether there is contact with the panel. The ADC outputs
#                                for coordinates not scaled. The contact reading is set to true when the ADC output is greater than zero and 
#                                false otherwise. The outputs are scaled using constants which do not account for error in the touchpanel readings.
#            @return     A tuple containing the ADC output for the x and y coordinates of contact and a boolean indicating contact with the panel.                    
#        '''
#        		# Set ym as analog 
#        self.Ym.init(Pin.ANALOG) 
#        ## @brief an ADC object linked to pin Ym
#        adcYm = ADC(self.Ym) 
#        
#        		 # set Xp as high push pull 
#        # self.Xp.init(Pin.OUT_PP, pull = Pin.PULL_UP) 
#        self.Xp.init(Pin.OUT_PP) 
#        self.Xp.value(True)
#        
#        		# set Xm as low push pull 
#        # self.Xm.init(Pin.OUT_PP, pull = Pin.PULL_DOWN) 
#        self.Xm.init(Pin.OUT_PP) 
#        self.Xm.value(False)
#        		# float Yp 
#        self.Yp.init(Pin.IN) 
#        ## @brief The ADC output for the x-coordinate of contact
#        self.scanX = adcYm.read()#/4096)*176 - 88 
#        
#        	# Set Xm as analog 
#        self.Xm.init(Pin.ANALOG) 
#        ## @brief an ADC object linked to pin Xm
#        adcXm = ADC(self.Xm) 
#        		# set Yp as high push pull 
#        self.Yp.init(Pin.OUT_PP) 
#        self.Yp.value(True)
#        
#        		# set Ym as low push pull 
#        self.Ym.init(Pin.OUT_PP) 
#        self.Ym.value(False)
#        
#        		# float Xp 
#        self.Xp.init(Pin.IN) 
#
#        ## @brief The ADC output for the x-coordinate of contact
#        self.scanY = adcXm.read()#/4096)*100 - 50
#        
#        		# Set Ym as analog 
#        self.Ym.init(Pin.ANALOG) 
#        adcYm = ADC(self.Ym) 
#        
#        		# set Yp as high push pull 
#        self.Yp.init(Pin.OUT_PP) 
#        self.Yp.value(True)
#        
#        		# set Xm as low push pull 
#        self.Xm.init(Pin.OUT_PP) 
#        self.Xm.value(False)
#        
#        		# float Xp 
#        self.Xp.init(Pin.IN) 
#        
#        ## @brief A boolean indicating contact with the touch panel
#        self.scanZ = adcYm.read() <4000
#        
#        #return('no check')
#        return (self.scanX, self.scanY, self.scanZ)
 

    
    # def scanXYZ_raw(self):
    #     '''!@brief      Returns the ADC values for x and y coordinates and touch (z axis)
    #         @details    This method assigns four pins attached to a resistive touch panel to push-pull high or low or analog setting
    #                     in order to measure the x coordinate, y coordinate, and whether there is contact with the panel. The ADC outputs
    #                     for coordinates go from 0 to 4095 repesenting the entie length of the panel an the contact reading is 0 if there is no
    #                     contact and some arbitrary number if there is
    #     '''
    #     self.Yp.init(self._alg) 
    #     adcYp = ADC(self.Yp)
    #     self.Ym.init(self._alg)
    #     self.Xp.init(self._ppOut,True)        
    #     self.Xm.init(self._ppOut,False)         
    #     scan_X = adcYp.read()
    #     self.Xm.init(self._alg)        
    #     self.Ym.init(self._ppOut,False)
    #     adcXm = ADC(self.Xm)           
    #     scan_Z = adcYp.read()
    #     self.Yp.init(self._ppOut,True)
    #     self.Xp.init(self._alg)     
    #     scan_Y = adcXm.read()
    #     return(scan_X,scan_Y,scan_Z)
        
    def scanXYZ(self):
        '''!@brief      Returns the approximate x and y coordinates of a touch (in millimeters) and a boolean representing contact
            @details    This method assigns four pins attached to a resistive touch panel to push-pull high or low or analog setting
                        in order to measure the x coordinate, y coordinate, and whether there is contact with the panel. The ADC outputs
                        for coordinates are not scale. The contact reading is true when the ADC output is less than 4000 and 
                        false otherwise. The outputs are scaled using constants which do not account for error in the touchpanel readings.
        '''
        self.Yp.init(self._alg) 
        self.Ym.init(self._alg)
        adcYm = ADC(self.Ym)
        self.Xp.init(self._ppOut,True)        
        self.Xm.init(self._ppOut,False)         
        scan_X = adcYm.read() *1.0
        self.Xm.init(self._alg)  
        adcXm = ADC(self.Xm)
        self.Ym.init(self._ppOut,False)    
        scan_Z = adcXm.read() < 4000
        self.Yp.init(self._ppOut,True)
        self.Xp.init(self._alg)     
        scan_Y = adcXm.read()*1.0
        return(scan_X,scan_Y,scan_Z)
