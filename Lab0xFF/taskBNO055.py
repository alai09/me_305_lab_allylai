'''!@file       taskBNO055.py
    @brief      A generator function to create and interact with a BNO055 IMU object
    @details    The file instantiates a single BNO055 object for coordinate definition.
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb import I2C
import os
import BNO055


def taskBNO055Fcn(taskName, period, eulerangles, euler_call, ang_vel, ang_call, cal_call, coef_call, set_call):
    '''!@brief                  A generator function updates readings from a BNO055 IMU
        @details                The task runs as a generator function and requires a
                                task name and interval to be specified. During each run of the function, IMU readings
                                are used to updated shared attributes for the platform position and velocity
        @param taskName         The name of the task as a string.
        @param period           The task interval or period specified as an integer
                                number of microseconds.
        @param eulerangles      A tuple of the euler angles of the platform measured by an IMU
        @param euler_call       A flag indicating whether euler angles of the platform are requested to be recorded
        @param ang_vel          Values for platform angular velocity measured from an IMU
        @param ang_call         A flag indicating whether eangular velocities of the platform are requested to be recorded
        @param cal_call         A flag indicating whether calibration coefficients of the IMU are requested to be recorded
        @param cal_call         A flag indicating whether calibration coefficients of the IMU are requested to be recorded
        @param coef_call        A flag indicating whether angular velocities of the platform are requested to be recorded
        @param set_call         A flag which idicates whether IMU coefficients are to be written to an IMU
    '''
    
    ## @brief The encoder object which is referenced by each task
    BNO = BNO055.BNO055(I2C(1, I2C.CONTROLLER))  
    
    ## @brief A variable representing the current state of the program
    state = 0
    
    ## @brief A timestamp, in microseconds, indicating the time when the function is first initialized.
    start_time = ticks_us()
    
    ## @brief A timestamp, in microseconds, indicating when the next iteration of the generator must run.
    next_time = ticks_add(start_time, period)
    

## TIMER START
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
## TIMER END


            ## Check if calibration file exists        
            if state == 0:
                ##If Calib file does exist
                if "IMU_cal_coeffs.txt" in os.listdir():
                    print("Has IMU cal file, writing coefficients to IMU")
                    state = 2
                    
                ## If Calib file does not exist
                else:
                    cal_call.write(True)
                    state = 1
                    
            ## Uncalibrated, perform calibration
            elif state == 1:
                
                if cal_call.read() == True:
                    print(BNO.getCalStatus())
                    cal_call.write(False)
                    state = 2
                
                

            ## The working class for the IMU               
            elif state == 2:
                
                eulerangles.write(BNO.readEuler())
                ang_vel.write(BNO.readAngVel())
                

                if coef_call.read() == True:
                    print(BNO.getCalCoeff())
                    coef_call.write(False)
                
                elif set_call.read() == True:
                    BNO.writeCalToIMU()
                    set_call.write(False)
                
                elif cal_call.read() == True:
                    print(BNO.getCalStatus())
                    cal_call.write(False)
                 
                #Use to continuously print the euler angles to calibrate IMU offset with level   
                print(BNO.readEuler())
          
            
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")    
            yield 'Yield'
                
        else:
            yield None
            