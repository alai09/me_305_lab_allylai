'''!@file       taskTouchPanel.py
    @brief      A generator to implement Touch Panel Driver tasks as part of an FSM.
    @details    During each iteration the generator initiates actions based 
                on the entered sampling period and user inputs. The Touch Panel Task interacts
                with controller task to provide readings for contact position.
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb import Pin, USB_VCP
import os
import touchpaneldrv
from ulab import numpy as np

def taskTouchPanelFcn(taskName, period, beta, touch_cal, touch_has_cal, f_Flag, ball_vel, ball_coords, collect_Flag):
    '''!@brief                  A generator function which reads and filters or transforms touch panel data
        @details                The task runs as a generator function and requires a
                                task name and interval to be specified. During each run, the task updates the ball coordinates and calculates estimated velocity.
                                The task also contains an alpha-beta filter which can be optionally used on the data before it is written to the input share objects.
        @param taskName         The name of the task as a string.
        @param period           The task interval or period specified as an integer
                                number of microseconds.
        @param beta             A share used to store the beta matrix which converts ADC readings of the touchpanel coordinates
        @param touch_cal        A flag which indicates that calibration of the touch panel is requested
        @param touch_has_cal    A flag which indicates whether the touch panel is currently calibrated
        @param f_Flag           A flag indicating whether values for the touch panel contact position and velocity should be filtered with an alpha-beta filter
        @param ball_vel         Tuple containing x and y velocity of touch panel contact
        @param ball_coords      Tuple containing x and y coordinates of touch panel contact
        @param collect_Flag     A flag that indicates that the user wants to collect touchpanel and duty cycle data
    '''
        
    Touchpanel = touchpaneldrv.TouchPanelDrv(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0)  
    # count = 0
    x_pos = 0
    y_pos = 0
    x_hat = 0
    y_hat = 0
    
    vx_hat = 0
    vy_hat = 0
    # alpha and beta between 0 and 1; start with alpha = 0.85 and beta = 0.005
    alpha = 0.80
    beta2 = 0.51
    state = 0
    count = 0
    last_x_pos = 0
    last_y_pos = 0
    serport = USB_VCP()
    X = np.array([[0,0,0], [0,0,0], [0,0,0], [0,0,0], [0,0,0]])
    
    
    ## START Timing Block 
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
        
        over_T = ticks_diff(current_time,next_time)
        if over_T >= 0:
            
            ## @brief Time difference between measurements of position
            Ts = (period + over_T)/1000000
        
            next_time = ticks_add(current_time, period)
            ## END Timing Block
            if state == 0:
                state = 1
            
            if state == 1:
                
                if ("touch_panel_cal_coeffs3.txt" in os.listdir()) & (count == 0):
                    
                    touch_has_cal.write(True)
                    print('Touch Panel calibration file found, writing file to system')
                    print('Delete "touch_panel_cal_coeffs.txt" file to re-calibrate')
                    print('Touch Panel Coefficients listed below:')
                    with open("touch_panel_cal_coeffs3.txt", "r") as f:
                        str_list = f.read()
                    csv_list = str_list.split(",")
                    print(csv_list)
                    myArray = np.array([  [float(csv_list[0]), float(csv_list[1])], [float(csv_list[2]), float(csv_list[3])], [float(csv_list[4]), float(csv_list[5])]  ])
                    beta.write(myArray)
                    state = 3
                    print
                elif (touch_cal.read() == True) & (touch_has_cal.read() == False):
                    print('Touch the panel in the order specified below. Hit enter while holding the point')
                    print('1---------+--------2')
                    print('|    |    |    |   |')
                    print('|---------5--------|')
                    print('|    |    |    |   |')
                    print('4---------+--------3')
                    state = 2
                
                elif touch_has_cal.read() == True:
                    state = 1
                elif f_Flag.read() == True:
                    state = 4   
                
                
                    
            if state == 2:
                Y = np.array([[-80, 40], [80, 40], [80, -40], [-80, -40], [0,0]])
                
                
                if serport.any():
                    Char = serport.read(1).decode()
                    if Char == '\r':
                        X[count] = (Touchpanel.scanXYZ()[0], Touchpanel.scanXYZ()[1],1)
                        print(X)
                        count += 1
                        
                    if count == 5:
                        beta1 = np.linalg.inv(np.dot(X.transpose(), X))
                        beta2 = np.dot(X.transpose(), Y)
                        beta.write(np.dot(beta1,beta2))
                        print('Writing coefficients to file')
                        with open("touch_panel_cal_coeffs3.txt", "w") as f:
                            f.write(    str(beta.read()[0,0]) + ',' + str(beta.read()[0,1]) + ',' + str(beta.read()[1,0]) + ',' + str(beta.read()[1,1]) + ',' + str(beta.read()[2,0]) + ',' + str(beta.read()[2,1]))                            
                       
                        
                        touch_cal.write(False)
                        touch_has_cal.write(True)
                        state = 3
                  
                        
            if state == 3:
                
                 x_pos = beta.read()[0,0]*Touchpanel.scanXYZ()[0]  +  beta.read()[0,1]*Touchpanel.scanXYZ()[1]  +  beta.read()[2,0]
                 vx = (x_pos - last_x_pos)/Ts
                 y_pos = beta.read()[1,1]*Touchpanel.scanXYZ()[1]  +  beta.read()[1,0]*Touchpanel.scanXYZ()[0]  +  beta.read()[2,1]
                 vy = (y_pos - last_y_pos)/Ts
                 ball_coords.write([x_pos,y_pos,Touchpanel.scanXYZ()[2]])
                 ball_vel.write([vx,vy])
                 last_x_pos = x_pos
                 last_y_pos = y_pos
                 #print(Touchpanel.scanXYZ())
                 #print(ball_coords.read())
                 if Touchpanel.scanXYZ()[2] == True:
                     #print(vx, vy)
                     yield None
                 if f_Flag.read() == True:
                     state = 4
                 
                
                 
                 
            if state == 4:              
                x_pos = beta.read()[0,0]*Touchpanel.scanXYZ()[0]  +  beta.read()[0,1]*Touchpanel.scanXYZ()[1]  +  beta.read()[2,0]
                vx = (x_pos - last_x_pos)/Ts
                y_pos = beta.read()[1,1]*Touchpanel.scanXYZ()[1]  +  beta.read()[1,0]*Touchpanel.scanXYZ()[0]  +  beta.read()[2,1]
                vy = (y_pos - last_y_pos)/Ts
                
                if Touchpanel.scanXYZ()[2] == True:
                    x_hat = x_hat + alpha*(x_pos - x_hat) + Ts*vx_hat
                    y_hat = y_hat + alpha*(y_pos - y_hat) + Ts*vy_hat
                    vx_hat = vx_hat + (beta2/Ts)*(x_pos - x_hat)
                    vy_hat = vy_hat + (beta2/Ts)*(y_pos - y_hat)
                ball_coords.write([x_hat,y_hat,Touchpanel.scanXYZ()[2]])
                ball_vel.write([vx_hat,vy_hat])
                last_x_pos = x_pos
                last_y_pos = y_pos
                #                 print(Touchpanel.scanXYZ())
                #                 if Touchpanel.scanXYZ()[2] == True:
                #                     print(x_hat,y_hat)
                 #print(Touchpanel.scanXYZ())
                 #print(Touchpanel.scanXYZ_raw())
                if f_Flag.read() == False:
                    state = 3
                #print("next: ",next_time)
                #print("current: ",current_time)
                #print(Ts)
                yield None

            
            else:
                yield None  

                     
                        
