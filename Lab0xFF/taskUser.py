'''!@file       taskUser.py
    @brief      A generator to implement UI tasks as part of an FSM.
    @details    During each iteration the generator initiates actions based 
                on the entered sampling period and user inputs. The taskUser interacts
                with the taskMotor and taskEncoder classes to translate user input to
                interaction with hardware. 
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP
import os
# import array
# from ulab import numpy as np

def taskUserFcn(taskName, period, motor_num, eulerangles, euler_call, ang_vel, ang_call, cal_call, m_Flag, duty_cycle1, duty_cycle2, w_Flag, ref_theta, meas_theta, kp, kd, coef_call, set_call, ki , beta, touch_cal, touch_has_cal, f_Flag, collect_Flag, s_Flag):
    '''!@brief                  A generator function which returns a value for state related to a UI task
        @details                The task runs as a generator function and requires a
                                task name and interval to be specified.
        @param taskName         The name of the task as a string.
        @param period           The task interval or period specified as an integer
                                number of microseconds.
        @param motor_num        An integer indicating which motor is to be controlled if a single motor duty cycle is being set
        @param eulerangles      A tuple of the euler angles of the platform measured by an IMU
        @param euler_call       A flag indicating whether euler angles of the platform are requested to be recorded
        @param ang_vel          Values for platform angular velocity measured from an IMU
        @param ang_call         A flag indicating whether eangular velocities of the platform are requested to be recorded
        @param cal_call         A flag indicating whether calibration coefficients of the IMU are requested to be recorded
        @param m_Flag           A flag indicating whether it is requested to set the duty cycle of one of the motors to a certain value
        @param duty_cycle1      A value for the duty cycle of motor 1
        @param duty_cycle2      A value for the duty cycle of motor 2
        @param w_Flag           A flag that indicates whether the user has enabled closed loop control of the motors
        @param ref_theta        A value indicating the desired angular position of the platform for each closed-loop controller
        @param meas_theta       A value indicating the measured angular position of the platform from an IMU
        @param kp               Values for inner and outer loop proportional controller gains (respectively)
        @param kd               Values for inner and outer loop derivative controller gains (respectively)
        @param coef_call        A flag indicating whether angular velocities of the platform are requested to be recorded
        @param set_call         A flag which idicates whether IMU coefficients are to be written to an IMU
        @param ki               Values for inner and outer loop integral controller gains (respectively)
        @param beta             A share used to store the beta matrix which converts ADC readings of the touchpanel coordinates
        @param touch_cal        A flag which indicates that calibration of the touch panel is requested
        @param touch_has_cal    A flag which indicates whether the touch panel is currently calibrated
        @param f_Flag           A flag indicating whether values for the touch panel contact position and velocity should be filtered with an alpha-beta filter
        @param collect_Flag     A flag that indicates that the user wants to collect touchpanel and duty cycle data
        @param s_Flag           A flag indicating that the user has pressed the 's' key to end an action
    '''
    print('stuck')
    ## @brief The state that the UI is currently in.
    state = 0
    
    # ## @brief A counter variable used for data collection. 
    # count = 0 
    
    # ## @brief Variable for time used in data collection
    # time = 0.00
    
    # ## @brief An array for data used in data collection
    # DatArray = array.array('l',[])
    
    # ## @brief An array for time data used in data collection
    # timeArray = array.array('f', [])
    # X_array = array.array('f', [])
    
    # ## @brief A boolean used to indicate whether the duty cycle is being set or data is being collected
    # collecting = False
    # ## @brief A number used to keep track of the sum of all the velocities sampled to calculate average velocity
    # velo_sum = 0
    # ## @brief An integer used to keep track of how many velocities have been sampled to calculate average velocity
    # v_count = 0
    # ## @brief An array for duty cycle specified data collection
    # DutyArray = array.array('l',[])
    # ## @brief An array for average velocity data used in data collection
    # VelocityArray = array.array('f', [])
    
 
    ## @brief The time that the UI tasks started. 
    start_time = ticks_us()
    
    ## @brief The start time plus the period indicating when the next cycle of UI begins
    next_time = ticks_add(start_time, period)
    
    ## @brief A (virtual) serial port object used for getting characters cooperatively.
    serport = USB_VCP()
    
    print('+-------------------------------------+')
    print('| Welcome to the Ball Balancing       |') 
    print('| Platforms User Interface            |')
    print('|-------------------------------------|')
    print('| To use the interface, press:        |')
    print('| "P" to print Euler angles from the  |') 
    print('|         IMU                         |')
    print('| "V" to print angular velocity from  |')
    print('|         the IMU                     |')
    print('| "U" to print calibration status     |')
    print('| "C" to write calibration coeffs     |')
    print('|         to IMU                      |')
    print('| "Q" to calibrate touch panel        |')
    print('| "F" to filter touch panel readings  |')
    print('| "m" to enter duty cycle for motor 1 |')
    print('| "M" to enter duty cycle for motor 2 |')   
    print('| "S" to stop data collection or test |')
    print('|      interface prematurely          |')
    print('| "H" to print this help menu         |')
    print('+-------------------------------------+') 
    print('| Closed Loop Controller Functions:   |')
    print('|                                     |')
    print('| press "k" to enter new inner loop   |')
    print('|               gain values           |')
    print('| press "K" to enter new outer loop   |')
    print('|               gain values           |')    
    print('| press "W" to enable or disable      |')
    print('|           platform balancing        |')
    print('| press "J" to collect data for 15    |')
    print('|               seconds               |')
    print('+-------------------------------------+')
    
    if ("touch_panel_cal_coeffs3.txt" not in os.listdir()):
        print("Touchpanel not calibrated, please press 'Q' to begin calibration")
      
    while True:
        ## @brief The current time.
        current_time = ticks_us()
   
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            
            if state == 0:
          
                state = 1  
  
            elif state == 1:
                
                if serport.any():
                    ## @brief The character input for UI.
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'p','P'}:
                        print('Typed P for Euler Angles')
                        euler_call.write(True)
                        print(eulerangles.read())
                        
                    if charIn in {'v','V'}:
                        print('Typed V for Angular Velocity')
                        ang_call.write(True)
                        print(ang_vel.read())                    
                        
                    if charIn in {'u','U'}:
                        print('Typed U for Calibration status')
                        cal_call.write(True)
                        
                    if charIn in {'C','c'}:
                        print('Typed C')
                        set_call.write(True)
                        
                    if charIn in {'Q','q'}:
                        touch_cal.write(True)
                         
                    if charIn in {'m','M'}:
                        print(charIn)
                        if charIn == 'm':
                            motor_num.write(1)
                        else:
                            motor_num.write(2)
                        state = 5     
                        
                    if charIn in {'s','S'}:
                        if collect_Flag.read() == True:
                            s_Flag.write(True)
                        else:
                            print('Data is not being collected, press J to start data collection')
                            
                    if charIn in {'h','H'}:
                        print('+-------------------------------------+')
                        print('| Welcome to the Ball Balancing       |') 
                        print('| Platforms User Interface            |')
                        print('|-------------------------------------|')
                        print('| To use the interface, press:        |')
                        print('| "P" to print Euler angles from the  |') 
                        print('|         IMU                         |')
                        print('| "V" to print angular velocity from  |')
                        print('|         the IMU                     |')
                        print('| "U" to print calibration status     |')
                        print('| "C" to write calibration coeffs     |')
                        print('|         to IMU                      |')
                        print('| "Q" to calibrate touch panel        |')
                        print('| "F" to filter touch panel readings  |')
                        print('| "m" to enter duty cycle for motor 1 |')
                        print('| "M" to enter duty cycle for motor 2 |')   
                        print('| "S" to stop data collection or test |')
                        print('|      interface prematurely          |')
                        print('| "H" to print this help menu         |')
                        print('+-------------------------------------+') 
                        print('| Closed Loop Controller Functions:   |')
                        print('|                                     |')
                        print('| press "k" to enter new inner loop   |')
                        print('|               gain values           |')
                        print('| press "K" to enter new outer loop   |')
                        print('|               gain values           |')    
                        print('| press "W" to enable or disable      |')
                        print('|           platform balancing        |')
                        print('| press "J" to collect data for 15    |')
                        print('|               seconds               |')
                        print('+-------------------------------------+')
                                
                    elif charIn in {'k','K'}:
                        
                        buffer = ''
                        oldkp = kp.read()
                        if charIn == 'k':
                            print(f'Inner loop Prop. gain is currently {oldkp[0]}')
                        if charIn == 'K':
                            print(f'Outer loop Prop. gain is currently {oldkp[1]}')
                        print('Enter a new gain value')
                        while True:
                            if serport.any():
                                Char = serport.read(1).decode()
                                if Char.isdigit():
                                    buffer += Char
                                if (Char == '-') and (len(buffer) == 0):
                                    buffer += Char
                                if (Char == '.') and (buffer not in {'.'}):
                                    buffer += Char
                                if (Char == '\b') or (Char == '\x08'):
                                    if len(buffer) != 0:
                                        buffer(-1).remove
                                if (Char == '\r') and (len(buffer) > 0):
                                    buffer = str(buffer) 
                                    if charIn == 'k':
                                        oldkp[0] = float(buffer)
                                        kp.write(oldkp)
                                    if charIn == 'K':
                                        oldkp[1] = float(buffer)
                                        kp.write(oldkp)
                                    print(f'Proportional Gain set to: {oldkp}')
                                    break
                        print('Enter a derivative gain')
                        buffer = ''
                        oldkd = kd.read()
                        if charIn == 'k':
                            print(f'Inner loop Deriv. gain is currently {oldkd[0]}')
                        if charIn == 'K':
                            print(f'Outer loop Deriv. gain is currently {oldkd[1]}')
                        while True:
                            if serport.any():
                                Char = serport.read(1).decode()
                                if Char.isdigit():
                                    buffer += Char
                                if (Char == '-') and (len(buffer) == 0):
                                    buffer += Char
                                if (Char == '.') and (buffer not in {'.'}):
                                    buffer += Char
                                if (Char == '\b') or (Char == '\x08'):
                                    if len(buffer) != 0:
                                        buffer(-1).remove
                                if (Char == '\r') and (len(buffer) > 0):
                                    buffer = str(buffer) 
                                    if charIn == 'k':
                                        oldkd[0] = float(buffer)
                                        kd.write(oldkd)
                                    if charIn == 'K':
                                        oldkd[1] = float(buffer)
                                        kd.write(oldkd)
                                    print(f'Derivative Gain set to: {oldkd}')
                                    break
                        print('Enter an integral gain')
                        buffer = ''
                        oldki = ki.read()
                        if charIn == 'k':
                            print(f'Inner loop Int. gain is currently {oldki[0]}')
                        if charIn == 'K':
                            print(f'Outer loop Int. gain is currently {oldki[1]}')
                        while True:
                            if serport.any():
                                Char = serport.read(1).decode()
                                if Char.isdigit():
                                    buffer += Char
                                if (Char == '-') and (len(buffer) == 0):
                                    buffer += Char
                                if (Char == '.') and (buffer not in {'.'}):
                                    buffer += Char
                                if (Char == '\b') or (Char == '\x08'):
                                    if len(buffer) != 0:
                                        buffer(-1).remove
                                if (Char == '\r') and (len(buffer) > 0):
                                    buffer = str(buffer) 
                                    if charIn == 'k':
                                        oldki[0] = float(buffer)
                                        ki.write(oldki)
                                    if charIn == 'K':
                                        oldki[1] = float(buffer)
                                        ki.write(oldki)
                                    print(f'Integral Gain set to: {oldki}')
                                    break
                      
                                
                    elif charIn in {'w','W'}:
                        if w_Flag.read() == True:
                            print('Closed loop OFF')
                            duty_cycle1.write(0)
                            duty_cycle2.write(0)
                            w_Flag.write(False)
                        elif w_Flag.read() == False:
                            print('Closed loop ON')
                            w_Flag.write(True)
                            
                    elif charIn in {'f','F'}:
                        if f_Flag.read() == True:
                            print('Touch Panel Filter Turned OFF')
                            f_Flag.write(False)
                        elif f_Flag.read() == False:
                            print('Touch Panel Filter Turned ON')
                            f_Flag.write(True)
                            
                    elif charIn in {'j','J'}:
                        collect_Flag.write(True)
                        print('--------------------Data Collection Start--------------------')
                        print(' ---X pos-----Y pos-----X vel-----Y vel-----DC1-----DC2---')
                            
                 # change motor duty cycle
            elif state == 5:
                print('Please enter a duty cycle between -100 and 100')
                buffer = ''
                while True:
                    if serport.any():
                        Char = serport.read(1).decode()
                        if Char.isdigit():
                            buffer += Char
                        if (Char == '-') and (len(buffer) == 0):
                            buffer += Char
                        if (Char == '\b') or (Char == '\x08'):
                            if len(buffer) != 0:
                                buffer(-1).remove
                        if Char == '\r':
                            buffer = str(buffer) 
                            try:
                                if (int(buffer) > 100) or (int(buffer) < -100):
                                    print('Duty cycle out of range')
                                    buffer = ''
                                    pass
                                else:
                                    if motor_num.read() == 1:
                                        duty_cycle1.write(int(buffer))
                                    else: 
                                        duty_cycle2.write(int(buffer))
                                    m_Flag.write(True)
                                    print(f'Duty Cycle of Motor {motor_num.read()} Set to {buffer}')
                                    break
                            except:
                                print('Invalid Input for Duty Cycle')
                    if m_Flag.read() == False:
                        state = 1  
                        
#                


            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield state
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None
