'''!@file       taskController.py
    @brief      A generator to implement a closed-loop controller
    @details    During each iteration the generator recalculates duty cycle outputs
                based on a control loop with the goal of balancing a ball on the associated platform.
                The duty cycle outputs for two motors are calculated separately based on two PID control loops
                based on the orientation of the platform and location of the ball. Other aspects of the 
                control system are pictured in the block diagram below.
'''
# Add in block diagram

from time import ticks_us, ticks_add, ticks_diff
import ClosedLoop
import array

def taskControllerFcn(taskName, period, m_Flag, eulerangles, ang_vel, kp, kd, duty_cycle1, duty_cycle2, w_Flag, ki, ball_vel, ball_coords, collect_Flag, s_Flag):
    '''!@brief                  A generator function performs closed loop control to update inputs for motor duty cycles
        @details                The task runs as a generator function and requires a
                                task name and interval to be specified. A control system is used to recalculate duty cycles for two motors during each iteration of the function
                                to be applied to the motors within the motor task.
        @param taskName         The name of the task as a string.
        @param period           The task interval or period specified as an integer
                                number of microseconds.
        @param m_Flag           A flag indicating whether it is requested to set the duty cycle of one of the motors to a certain value
        @param eulerangles      A tuple of the euler angles of the platform measured by an IMU
        @param ang_vel          Values for platform angular velocity measured from an IMU
        @param kp               Values for inner and outer loop proportional controller gains (respectively)
        @param kd               Values for inner and outer loop derivative controller gains (respectively)
        @param duty_cycle1      A value for the duty cycle of motor 1
        @param duty_cycle2      A value for the duty cycle of motor 2
        @param w_Flag           A flag that indicates whether the user has enabled closed loop control of the motors
        @param ki               Values for inner and outer loop integral controller gains (respectively)
        @param ball_vel         Tuple containing x and y velocity of touch panel contact
        @param ball_coords      Tuple containing x and y coordinates of touch panel contact
        @param collect_Flag     A flag that indicates that the user wants to collect touchpanel and duty cycle data
        @param s_Flag           A flag indicating that the user has pressed the 's' key to end an action
    '''
    
    state = 0
       
    ## No Contact CLC
    innerX_no_ball = ClosedLoop.ClosedLoop(6, 0.3, 4) 
    innerY_no_ball = ClosedLoop.ClosedLoop(6, 0.3, 4)
    
    ##Contact CLC
    innerX = ClosedLoop.ClosedLoop(kp.read()[0], kd.read()[0], ki.read()[0] ) 
    innerY = ClosedLoop.ClosedLoop(kp.read()[0], kd.read()[0], ki.read()[0] ) 
    outerX = ClosedLoop.ClosedLoop(kp.read()[1]+0.015, kd.read()[1]-0.015, ki.read()[1] ) 
    outerY = ClosedLoop.ClosedLoop(kp.read()[1], kd.read()[1], ki.read()[1] ) 
    count = 0
    runs = 0

    
## TIMER START 
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
## TIMER END


            
            ## Exit initial zero state
            if state == 0:
                state = 1
                
           ## Disabled controller task 
            elif state == 1:
                ##User wants to run the ClosedLoop function
                if w_Flag.read() == True:
                    state = 2
                else:           
                    pass
            ## Enabled controller task        
            elif state == 2:

                if w_Flag.read() == False:                   
                    state = 1
                
                else:
                    
                    theta1 = outerX.run(0,ball_coords.read()[0],0,ball_vel.read()[0])                    
                    theta2 = outerY.run(0,ball_coords.read()[1],0,ball_vel.read()[1])
                    
                    ## SATURATION BLOCKS
                    if theta1 > 12:
                        theta1 = 12
                        
                    elif theta1 < -12:
                        theta1 = -12
                        
                        
                    if theta2 > 12:
                        theta2 = 12
                        
                    elif theta2 < -12:
                        theta2 = -12     
                    
                    #Add gravity offset(4 at the moment not tuned)
                    DC1 = (innerX.run(theta1, eulerangles.read()[2]+1.325, 0, ang_vel.read()[0])) 

                    ## Static Friction Block
                    if ang_vel.read()[0] > 0:
                        DC1 = DC1+10
                    elif ang_vel.read()[0] < 0:
                        DC1 = DC1-10
                    
                    #Add gravity offset(4 at the moment not tuned)
                    #Measured Euler Angle Offset from IMU is positive 1.125 around x axis (theta_y)
                    DC2 = (innerY.run(-theta2, eulerangles.read()[1]-0.25, 0, ang_vel.read()[1]))
                    
                    ## Static Friction Block
                    if ang_vel.read()[1] > 0:
                        DC2 = DC2+6
                    elif ang_vel.read()[1] < 0:
                        DC2 = DC2-6
                   
                    ## Contact Block
                    if (ball_coords.read()[2] == True):
                        count = 0
                        if (DC1 > 45):
                            DC1 = 45
                            duty_cycle1.write(DC1)     
                        elif (DC1 < -45):
                            DC1 = -45
                            duty_cycle1.write(DC1)
                        else:
                            duty_cycle1.write(DC1)
                  
                        if (DC2 > 45):
                            DC2 = 45
                            duty_cycle2.write(DC2) 
                        elif (DC2 < -45):
                            DC2 = -45
                            duty_cycle2.write(DC2)
                        else:
                            duty_cycle2.write(DC2)
                        count = 0
                    if collect_Flag.read() == True:
                        if runs < 2001/2:
#                            x_pos_Array[runs] = ball_coords.read()[0]
#                            y_pos_Array[runs] = ball_coords.read()[1]
#                            x_vel_Array[runs] = ball_vel.read()[0]
#                            y_vel_Array[runs] = ball_vel.read()[1]
#                            runs += 1                        
#                        if (runs >= 3001/2) | (s_Flag.read() == True):
#                            print('----------Start Unfiltered Data----------')
#                            for i,n,j,k in zip(x_pos_Array, y_pos_Array, x_vel_Array, y_vel_Array):
#                                print(str(i) + ', ' + "{:.2f}".format(n)+ ', ' + "{:.2f}".format(j)+ ', ' + "{:.2f}".format(k))
#                            print('----------End Data----------')
#                            runs = 0
#                            collect_Flag.write(False)
#                            s_Flag.write(False)

                            print(ball_coords.read()[0]," ",ball_coords.read()[1], " ", ball_vel.read()[0], " ", ball_vel.read()[1]," ",duty_cycle1.read(), " ", duty_cycle2.read())
                            runs += 1
                        if (runs >= 2001/2) | (s_Flag.read() == True):
                            print('--------------------Data Collection End--------------------')
                            runs = 0
                            collect_Flag.write(False)
                            s_Flag.write(False)
                    count += 1

                    ## No Contact Block
                    if  (count > 50) & (ball_coords.read()[2] == False):
                        
                        DC3 = innerX_no_ball.run(0, eulerangles.read()[2]+1.3125, 0, ang_vel.read()[0])
                        DC4 = innerY_no_ball.run(0, eulerangles.read()[1]-0.25, 0, ang_vel.read()[1])

                        #Saturation
                        if (DC3 > 45):
                            duty_cycle1.write(45)  
                        elif (DC3 < -45):
                            duty_cycle1.write(-45)
                        else: 
                            duty_cycle1.write(DC3)
                        
                        
                        if (DC4 > 45):
                            duty_cycle2.write(45)  
                        elif (DC4 < -45):
                            duty_cycle2.write(-45)
                        else:
                            duty_cycle2.write(DC4)
                        #print(DC3," ",DC4)
                        
                        #print("F ",ball_coords.read()[0]," ",ball_coords.read()[1], " ", ball_vel.read()[0], " ", ball_vel.read()[1]," ",duty_cycle1.read(), " ", duty_cycle2.read())

            
        else:
            yield None                    
                        
    