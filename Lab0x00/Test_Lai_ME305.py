# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 08:20:05 2022

@author: melab15
"""

def my_add(a, b):
    return a + b

#if statement only runs code if the file is run as a main program, (clicking run above)
#can also be structured by having main function and just calling the function inside the if statement
if __name__ == '__main__':
    # Testing code
    my_add(4,5)
    
    