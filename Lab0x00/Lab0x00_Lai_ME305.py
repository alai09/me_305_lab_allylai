# -*- coding: utf-8 -*-
"""
@file 
Created on Thu Jan  6 08:20:05 2022

@author: melab15

Ally Lai
ME 305
Lab 0x00: Fibonacci


"""

#function takes in an index and returns the corresponding term of the ...
# ...fibonacci sequence assuming that the sequence starts with the number (0, 1)
def fib(idx):
    f1 = 0
    f2 = 1
    sum = 0
    #fib represents the current index of variable f1 in the fibonacci sequence
    fib = 0
    #the following loop iterates through each index in the fibonacci sequence by storing ...
    # ...the value of the current index and next number in the sequence and returns the value...
    # ...of f1 when the value of fib, or its index, equals the value of the input index
    while idx > fib:
        sum = f1 + f2
        f1 = f2
        f2 = sum
        fib += 1
    return f1

'''
when run as a main this file allows the user to input an index and recieve the
corresponding value in the fibonnaci sequence as many times as they would like
'''
if __name__ == '__main__':
    idx_input = 0
    fib_num = 0
    #main starts by prompting an input and repeats prompt until quit command 'q' is entered
    while idx_input != "q":
        idx_input = input("Enter an index to get the corresponding number in the Fibonnaci Sequence:")
        #the following statement is only true if the input only has numeric characters which excludes...
        #...negative numbers and decimals because of the "-" and "." characters as well as non-numbers
        if idx_input.isnumeric():
            #if the index is in correct format, it is input into the fib() function and printed
            idx = int(idx_input)
            print ('\nThe Fibonacci number at '
                   'index {:} is {:}.'.format(idx,fib(idx)))
            #another input is called which allows the user to input q and stop of the loop or continue inputting indices
            idx_input = input("Press \"q\" to quit the program or any other key to try another index:")
        else: #if input is invalid a statement indicating so is printed and the loop is repeated to prompt another input
            print("\nThe value entered is not in the correct format; please enter a non-negative integer as the desired index")
        
    