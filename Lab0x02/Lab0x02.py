'''!@file                    Lab0x02.py
    @brief                   
    @details                 
    
    @author                  Ally Lai
    @author                  Jason Hu
    @copyright               This file has no license
    
    @date                    January 19, 2022
    @package                 Lab0x02

'''

import pyb
import time
import math


if __name__ == '__main__':
    
    
    pinB6 = pyb.Pin(pyb.Pin.cpu.PB6, pyb.Pin.OUT_PP)
    pinB7 = pyb.Pin(pyb.Pin.cpu.PB7, pyb.Pin.OUT_PP)
    
    
    tim4 = pyb.Timer(4, freq = 20000)
    
    t4ch1 = tim4.channel(1, pyb.Timer.ENC_A, pin=pinB6)
    t4ch2 = tim4.channel(2, pyb.Timer.ENC_B, pin=pinB7)
    
    while True:
        print(tim4.counter());
        pyb.delay(100)
        

