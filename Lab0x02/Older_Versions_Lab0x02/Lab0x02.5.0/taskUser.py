'''!@file       taskUser.py
    @brief      A generator to implement UI tasks as part of an FSM.
    @details    During each iteration the generator initiates actions based 
                on the entered sampling period and user inputs.
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP

def taskUserFcn(taskName, period, z_Flag, p_Flag ,d_Flag, g_Flag, s_Flag):
    '''!@brief              A generator function which returns a value for state related to a UI task
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param z_Flag       A Share object which is used to indicate that an 
                            action related to entering the "z" key should be performed
        @param p_Flag       A Share object which is used to indicate that an 
                            action related to entering the "p" key should be performed
        @param d_Flag       A Share object which is used to indicate that an 
                            action related to entering the "d" key should be performed                    
        @param g_Flag       A Share object which is used to indicate that an 
                            action related to entering the "g" key should be performed                    
        @param s_Flag       A Share object which is used to indicate that an 
                            action related to entering the "s" key should be performed
    '''

    state = 0

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    serport = USB_VCP()
    
    print('+------------------------------------+')
    print('| Welcome to the Encoder Interface   |')
    print('|------------------------------------|')
    print('| To use the interface, press:       |')
    print('| "Z" to zero the position           |')
    print('| "P" to get position                |')
    print('| "D" to get the delta value         |')
    print('| "G" to collect data for 30 seconds |')
    print('| "S" to stop data collection        |')
    print('|      prematurely                   |')
    print('+------------------------------------+')    
      
    while True:
        current_time = ticks_us()
    
   
        if ticks_diff(current_time,next_time) >= 0:
        

            next_time = ticks_add(next_time, period)
            

            if state == 0:
          
                state = 1  
   
            elif state == 1:
 
                if serport.any():
                    charIn = g
                    
                    if charIn in {'z','Z'}:
                        print('Typed Z')
                        state = 2
                        z_Flag.write(True)
                    elif charIn in {'p','P'}:
                        print('Typed P')
                        state = 3
                        p_Flag.write(True)
                    elif charIn in {'d','D'}:
                        print('Typed D')
                        state = 4
                        d_Flag.write(True)
                    elif charIn in {'g','G'}:
                        print('Typed G')
                        state = 5
                        g_Flag.write(True)
                    elif charIn in {'s','S'}:
                        if g_Flag.read() == False:
                            print('Data is not being collected, press G to start data collection')
                        
                        
                    
            # for input z zero encoder position
            elif state == 2:
                
                if z_Flag.read() == False:
                    state = 1 # transition to state 0
                    
            elif state == 3:
                print(taskEncoder.enc1.getPosition())
                if p_Flag.read() == False:
                    
                    state = 1 # transition to state 0
                    
                    
            elif state == 4:
                # If a character is waiting, read it
                if d_Flag.read() == False:
                    
                    state = 1 # transition to state 0
                    
            elif state == 5:
                # If a character is waiting, read it
                if g_Flag.read() == False:
                    
                    state = 1 # transition to state 0
                    

                        
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield state
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None
