'''!@file       taskEncoder.py
    @brief      
    @details    
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP
import encoder
import array
import os


def taskEncoderFcn(taskName, period, z_Flag, p_Flag ,d_Flag, g_Flag, s_Flag):
    '''!@brief              
        @details            

    '''
    
    enc1 = encoder.Encoder('B',4)
    
    state = 0
    count = 0 
    time = 0.00
    DatArray = array.array('l',[enc1.get_position()])
    timeArray = array.array('f', [0])
    
    # A timestamp, in microseconds, indicating when the next iteration of the
    # generator must run.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    # A (virtual) serial port object used for getting characters cooperatively.
    
    # The finite state machine must run indefinitely.
    while True:
        serport = USB_VCP()
        current_time = ticks_us()
    
   
        if ticks_diff(current_time,next_time) >= 0:
        

            next_time = ticks_add(next_time, period)
            
            if state == 0:
                state = 1
                
            elif state == 1:
                enc1.update()
                if z_Flag.read() == True:
                    state = 2
                if p_Flag.read() == True:
                    state = 3
                if d_Flag.read() == True:
                    state = 4
                if g_Flag.read() == True:
                    state = 5
                if s_Flag.read() == True:
                    state = 6
                    
            elif state == 2:
                enc1.zero()
                print("Encoder Position Set to Zero")
                z_Flag.write(False)
                state = 1
                
            elif state == 3:
                print(enc1.get_position())
                p_Flag.write(False)
                state = 1
                
            elif state == 4:
                print(enc1.get_delta())
                d_Flag.write(False)
                state = 1
                
            elif state == 5:
                if count <= 3001:
                    
                    enc1.update()
                    DatArray.append(enc1.get_position())
                    
                    timeArray.append(time)
                    
                    time += (1/100)
                    
                    count += 1
                    
                    
                    if serport.any() == True:
                        if serport.read(1).decode() == 's':
                            print("Data Collection Stopped")
                            g_Flag.write(False)
                            
                            DatArray = array.array('l',[enc1.get_position()])
                            timeArray = array.array('f', [0])
                            count = 0
                            time = 0
                            state = 1
                else:
                    g_Flag.write(False)
                    print("Data Collection Finished")
                    DatArray = array.array('l',[enc1.get_position()])
                    timeArray = array.array('f', [0])
                    
                    
                    state = 1
                    count = 0
                    time = 0
            
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")    
            yield enc1.get_position()
                
        else:
            yield None
                
        
    






    
#    # The state of the FSM to run on the next iteration of the generator.
#    state = 0
#        
#    # Welcome Statement
#    print("ME 305 Lab 0x02")
#    
#    # A timestamp, in microseconds, indicating when the next iteration of the
#    # generator must run.
#    start_time = ticks_us()
#    next_time = ticks_add(start_time, period)
#    
#    # A (virtual) serial port object used for getting characters cooperatively.
#    serport = USB_VCP()
#    
#    # The finite state machine must run indefinitely.
#    while True:
#
#        current_time = ticks_us()
#    
#   
#        if ticks_diff(current_time,next_time) >= 0:
#        
#
#            next_time = ticks_add(next_time, period)
#            
#            # State 0
#            if state == 0:
#                # If a character is waiting, read it
#                if serport.any():
#
#                    charIn = serport.read(1).decode()
#                    
#                    if charIn == '1':
#                        print("Transitioning to state 1")
#                        state = 1 # transition to state 1
#                    else:
#                        print(f"You typed {charIn} from state 0 at t={ticks_diff(current_time,start_time)/1e6}[s].")
#            
#            # State 1
#            elif state == 1:
#                # If a character is waiting, read it
#                if serport.any():
#                    # Read one character and decode it into a string
#                    charIn = serport.read(1).decode()
#                    
#                    if charIn == '0':
#                        print("Transitioning to state 0")
#                        state = 0 # transition to state 0
#                    elif charIn == '2':
#                        print("Transitioning to state 2")
#                        state = 2 # transition to state 2
#                    else:
#                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")
#                    
#            elif state == 2:
#                # If a character is waiting, read it
#                if serport.any():
#                    # Read one character and decode it into a string
#                    charIn = serport.read(1).decode()
#                    
#                    if charIn == '0':
#                        print("Transitioning to state 0")
#                        state = 0 # transition to state 0
#                    elif charIn == '2':
#                        print("Transitioning to state 2")
#                        state = 2 # transition to state 2
#                    else:
#                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")
#                        
#            else:
#                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
#            
#            # After a valid run of the state machine we can yield the state.
#            # This yielded value could be stored in the main loop below to trace
#            # the state transitions.
#            yield state
#        
#        # If the time has not come yet to run the task we can just exit early by
#        # yielding nothing.
#        else:
#            yield None
