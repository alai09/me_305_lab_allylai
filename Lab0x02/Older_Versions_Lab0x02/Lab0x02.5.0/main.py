'''!@file       main.py
    @brief      
    @details    
'''

import encoder, pyb, time, shares, taskUser, taskEncoder


z_Flag = shares.Share(False)
p_Flag = shares.Share(False)
d_Flag = shares.Share(False)
g_Flag = shares.Share(False)
s_Flag = shares.Share(False)

if __name__ == "__main__":
    '''!@brief              Constructs an empty queue of shared values
    '''
    
    taskList = [taskUser.taskUserFcn      ('Task User', 10_000, z_Flag, p_Flag ,d_Flag, g_Flag, s_Flag),
                taskEncoder.taskEncoderFcn('Task Encoder', 10_000, z_Flag, p_Flag ,d_Flag, g_Flag, s_Flag)]
    
    while True:
            try:            
                for task in taskList:
                    next(task)    
            except KeyboardInterrupt:
                break
    print("Program Terminated")