'''!@file       main.py
    @brief      
    @details    
'''

import encoder, pyb, time, shares, taskUser, taskEncoder


z_Flag = shares.Share(False)
position = shares.Share(False)
delta = shares.Share(False)
time = shares.Share(False)

if __name__ == "__main__":
    '''!@brief              Constructs an empty queue of shared values
    '''
    
    taskList = [taskUser.taskUserFcn      ('Task User', 10_000, z_Flag, position, delta, time),
                taskEncoder.taskEncoderFcn('Task Encoder', 10_000, z_Flag, position, delta, time)]
    
    while True:
            try:            
                for task in taskList:
                    next(task)    
            except KeyboardInterrupt:
                break
    print("Program Terminated")