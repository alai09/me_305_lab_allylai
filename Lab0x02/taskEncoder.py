'''!@file       taskEncoder.py
    @brief      A generator to update encoder attributes
    @details    The generator always updates the current position and delta of 
                the referenced encoder to the shared position and delta variables if the
                indicated time period has elapsed since the last run of the generator function.
                When flagged to do so the position of the encoder can be set to zero with the
                encoder attributes updating afterwards.
'''

from time import ticks_us, ticks_add, ticks_diff
import encoder


def taskEncoderFcn(taskName, period, z_Flag, position, delta):
    '''!@brief              A generator function which updates encoder attributes at the input frequency
        @details            The task runs as a generator function and with each run updates the delta and position
                            attributes of the encoder. If the z_Flag argument is true the position of the encoder
                            is set to zero.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param z_Flag       A Share object which is used to indicate that an 
                            action related to entering the "z" key should be performed
        @param position     A Share object respresenting the position attribute of the encoder
        @param delta        A Share object respresenting the delta attribute of the encoder
    '''
    ## @brief The encoder object which is referenced by each task
    enc1 = encoder.Encoder('B',4)
    
    ## @brief A variable representing the current state of the program
    state = 0

    
    ## @brief A timestamp, in microseconds, indicating when the next iteration of the generator must run.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    # The finite state machine must run indefinitely.
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            
            if state == 0:
                state = 1
                
            elif state == 1:
                if z_Flag.read() == True:
                    enc1.zero()
                    print("Encoder Position Set to Zero")
                    z_Flag.write(False)
                enc1.update()
                position.write(enc1.get_position())
                delta.write(enc1.get_delta())
            
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")    
            yield enc1.get_position()
                
        else:
            yield None
                
#    # The state of the FSM to run on the next iteration of the generator.
#    state = 0
#        
#    # Welcome Statement
#    print("ME 305 Lab 0x02")
#    
#    # A timestamp, in microseconds, indicating when the next iteration of the
#    # generator must run.
#    start_time = ticks_us()
#    next_time = ticks_add(start_time, period)
#    
#    # A (virtual) serial port object used for getting characters cooperatively.
#    serport = USB_VCP()
#    
#    # The finite state machine must run indefinitely.
#    while True:
#
#        current_time = ticks_us()
#    
#   
#        if ticks_diff(current_time,next_time) >= 0:
#        
#
#            next_time = ticks_add(next_time, period)
#            
#            # State 0
#            if state == 0:
#                # If a character is waiting, read it
#                if serport.any():
#
#                    charIn = serport.read(1).decode()
#                    
#                    if charIn == '1':
#                        print("Transitioning to state 1")
#                        state = 1 # transition to state 1
#                    else:
#                        print(f"You typed {charIn} from state 0 at t={ticks_diff(current_time,start_time)/1e6}[s].")
#            
#            # State 1
#            elif state == 1:
#                # If a character is waiting, read it
#                if serport.any():
#                    # Read one character and decode it into a string
#                    charIn = serport.read(1).decode()
#                    
#                    if charIn == '0':
#                        print("Transitioning to state 0")
#                        state = 0 # transition to state 0
#                    elif charIn == '2':
#                        print("Transitioning to state 2")
#                        state = 2 # transition to state 2
#                    else:
#                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")
#                    
#            elif state == 2:
#                # If a character is waiting, read it
#                if serport.any():
#                    # Read one character and decode it into a string
#                    charIn = serport.read(1).decode()
#                    
#                    if charIn == '0':
#                        print("Transitioning to state 0")
#                        state = 0 # transition to state 0
#                    elif charIn == '2':
#                        print("Transitioning to state 2")
#                        state = 2 # transition to state 2
#                    else:
#                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")
#                        
#            else:
#                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
#            
#            # After a valid run of the state machine we can yield the state.
#            # This yielded value could be stored in the main loop below to trace
#            # the state transitions.
#            yield state
#        
#        # If the time has not come yet to run the task we can just exit early by
#        # yielding nothing.
#        else:
#            yield None
