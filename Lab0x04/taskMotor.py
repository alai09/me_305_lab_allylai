'''!@file       taskMotor.py
    @brief      A generator create and interact with a motor driver
    @details    The file instantiates a single motor driver and two motors, 
                motor 1 and motor 2. The states of the function indicate whether the motor driver is
                enabled or a fault has been reported. TaskMotor implements commands to the motors or 
                motor driver based on user input interpreted in the taskUser file. This allows motor duty cycles
                to be changed and faults to be cleared pending taskUser approval. 
                

'''
                
    # @image      html Lab0x03STD.JPG "" width=1000
    #             The State Transition Diagram for the TaskUser file and UI Tasks. 

from time import ticks_us, ticks_add, ticks_diff
from pyb import Pin
import DRV8847

def taskMotorFcn(taskName, period, motor_num, m_Flag, w_Flag, duty_cycle, fault):
    '''!@brief              A generator function which updates encoder attributes at the input frequency
        @details            The task runs as a generator function and with each run updates the delta and position
                            attributes of the encoder. If the z_Flag argument is true the position of the encoder
                            is set to zero.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param motor_num    The number of the motor being controlled   
        @param m_Flag       A Share object which is used to indicate that the motor
                            duty cycle should be set to the current "duty_cycle" value
        @param duty_cycle   An input duty_cycle for the motor  
        @param fault        Boolean indicating that the motor has been disabled by a fault condition
    '''
    
    ## @brief The motor driver object used to create motor object
    motor_drv = DRV8847.DRV8847(3, Pin.cpu.A15, Pin.cpu.B2)
    ## @brief The motor object used to interface with motor 1
    mot_1 = motor_drv.motor(Pin.cpu.B4, Pin.cpu.B5)
    ## @brief The motor object used to interface with motor 2
    mot_2 = motor_drv.motor(Pin.cpu.B0, Pin.cpu.B1)
    
    ## @brief A variable representing the current state of the program
    state = 0

    
    ## @brief A timestamp, in microseconds, indicating when the next iteration of the generator must run.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            
            #Startup State
            if state == 0:
                state = 1
            
            # Motor Disabled
            if state == 1:            
                if motor_drv.hasfault == True:
                    fault.write(True)
                    state = 3
                if m_Flag.read() is True:
                    motor_drv.enable()
                    if motor_num.read() == 1:
                        mot_1.set_duty(duty_cycle.read())
                    elif motor_num.read() == 2:
                        mot_2.set_duty(duty_cycle.read())
                    m_Flag.write(False)
                    state = 2
                elif w_Flag.read() is True:
                    motor_drv.enable()
                    mot_1.set_duty(duty_cycle.read())
                    state = 2
                    

            # Motor Enabled
            elif state == 2:
                if motor_drv.hasfault == True:
                    fault.write(True)
                    state = 3
                elif m_Flag.read() is True:    
                    if motor_num.read() == 1:
                        mot_1.set_duty(duty_cycle.read())
                    elif motor_num.read() == 2:
                        mot_2.set_duty(duty_cycle.read())
                    m_Flag.write(False)
                elif  w_Flag.read() is True:
                    mot_1.set_duty(duty_cycle.read())
                    
            # Fault in the motor
            elif state == 3:
                if fault.read() == False:
                    # Duty cycle set back to 0 for the faulted motor
                    if motor_num.read() == 1:
                        mot_1.set_duty(0)
                    elif motor_num.read() == 2:
                        mot_2.set_duty(0)
                    motor_drv.enable()
                    state = 1
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")    
            yield 'Yield'
                
        else:
            yield None