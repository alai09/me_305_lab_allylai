'''!@file       DRV8847.py
    @brief      A class that impliments and alters motor objects and detects faults
                using a motor driver chip that is located on the STM32 Nucleo.
    @details    This class impliments the motor driver chip to control both motors as well
                as detect fault conditions that can arise from misuse. It creates motor objects, 
                then enables them for use with the taskMotor UI class. 
                
    @image      html Lab0x03DRV8847.JPG "" width=1000
                The State Transition Diagram for the DRV8847 class. 
                
'''
from motor import Motor
from pyb import Pin, ExtInt, delay, Timer

class DRV8847:
    '''!@brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                     motor driver and to create one or more objects of the
                     Motor class which can be used to perform motor
                     control.
    
                     Refer to the DRV8847 datasheet here:
                     https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    def __init__ (self, tim_num, nSLEEPpin, nFAULTpin):

        '''!@brief              Initializes and returns a DRV8847 object.
            @param tim_num      The PWM timer number that is to be used
                                with the DRV8847 chip. This timer is created when a DRV8847 object
                                is created and is used to control signals to the motors 
                                attached to the chip and is used to set the duty. 
            @param nSLEEPpin    The pin associated with the sleep function of the chip.
                                When this pin reads low, the chip is not in use. When it
                                reads high, the chipi is used to control the motors.
            @param nFAULTpin    The fault pin associated with misuse detection of the motor. 
                                This pin reads high when a fault, such as excess voltage or current
                                is detected. 
            @param fault        The fault flag that holds a boolean value. It is TRUE when a fault is
                                detected and FALSE when there is no fault or a fault has been cleared. 
        '''
        
        ## @brief The pin associated with the SLEEP function of the chip.
        self.nSLEEP = Pin(nSLEEPpin, mode=Pin.OUT_PP)
        
        ## @brief The pin associated with the FAULT detection of the chip.
        self.nFAULT = Pin(nFAULTpin, mode=Pin.OUT_PP)
        
        ## @brief The timer object associated with timing and duty cycle of the motor. 
        self.PWM_timer = Timer(tim_num, freq = 20_000)
        
        ## @brief A button interrupt variable that is used to detect faults and transition to the 
        #           fault callback function, fault_cb.
        self.ButtonInt = ExtInt(nFAULTpin, mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback=self.fault_cb)
        ## @brief A boolean which indicates whether a fault has occured
        self.hasfault = False
        
        pass
        
        
    def enable (self):
        '''!@brief      Brings the DRV8847 out of sleep mode.
            @details    Sets the sleep pin to high, bringing the chip out of 'hibernation'
                        and allowing it to control the motors. It also avoids any initial fault conditions
                        that may occur.
        '''
        self.ButtonInt.disable()
        self.nSLEEP.high()
        delay(5)
        self.ButtonInt.enable()
        self.hasfault = False
        pass
        

    def disable (self):
        '''!@brief      Puts the DRV8847 in sleep mode.
            @details    Sets the SLEEP pin to low, deactivating the driver chip. 
        '''
        self.nSLEEP.low()
        pass
        
    def fault_cb (self, IRQ_src):
        '''!@brief          Callback function to run on fault condition.
            @details        Upon fault detection, this method will change the fault flag, 
                            allowing the fault to be read across all files. 
            @param IRQ_src  The source of the interrupt request.
        '''
        self.hasfault = True
        self.disable()

        pass
    
    def motor (self, pin1, pin2):
        '''!@brief          Creates a DC motor object connected to the DRV8847.
            @param pin1     The first input pin for the motor object.
            @param pin2     The second input pin for the motor object.
            @return         An object of class Motor
        '''
     
        return Motor(self.PWM_timer, pin1, pin2)
       