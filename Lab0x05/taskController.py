from time import ticks_us, ticks_add, ticks_diff
import ClosedLoop

def taskControllerFcn(taskName, period, m_Flag, eulerangles, ang_vel, kp, kd, duty_cycle, duty_cycle2, w_Flag, ki):
    state = 0
    
##Creating ClosedLoop Object
    closed_loop = ClosedLoop.ClosedLoop()   
    ## Timing Block 
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            ## End Timing Block
            
            ## Exit initial zero state
            if state == 0:
                
                state = 1
                
           ## Disabled controller task 
            elif state == 1:
                ##User wants to run the ClosedLoop function
                if w_Flag.read() == True:
                    state = 2
                else:           
                    pass
            ## Enabled controller task        
            elif state == 2:

                if w_Flag.read() == False:
                    state = 1
                else:
                    duty_cycle.write(closed_loop.run(kp.read(), kd.read(), 0, eulerangles.read()[2], 0, ang_vel.read()[0]))
                    # print("Duty Cycle:")
                    # print(duty_cycle.read())
                    # print("Theta:")
                    # print(eulerangles.read()[2])
                    # print("Theta_dot:")
                    # print(ang_vel.read()[0])
    #                print(eulerangles.read()[1])
    #                print(ang_vel.read()[1])
                    
                    duty_cycle2.write(closed_loop.run(kp.read(), kd.read(), 0, eulerangles.read()[1], 0, ang_vel.read()[1]))
                #print(duty_cycle2.read())
                #m_Flag.write(True)
               
                
                
                ## User wants to change the KP value.
            
        else:
            yield None                    
                        
                        
                        
    