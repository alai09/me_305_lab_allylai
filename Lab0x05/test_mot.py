import motor
from pyb import Timer, Pin, delay

tim = Timer(3, freq = 20_000)
## @brief The motor object used to interface with motor 1
mot_1 = motor.Motor(tim, Pin.cpu.B4, Pin.cpu.B5)
## @brief The motor object used to interface with motor 2
mot_2 = motor.Motor(tim, Pin.cpu.B0, Pin.cpu.B1)

mot_1.set_duty(0)
delay(1000)
