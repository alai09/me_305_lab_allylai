'''!@file       BNO055.py
    @brief      
    @details    
'''
from pyb import I2C, delay
import struct


class BNO055:
    '''!@brief      
        @details    
    '''
    def __init__ (self, i2c_ref):
    
        '''!@brief   
        '''
        
        self.i2c = i2c_ref
        self.cal_bytes = bytearray([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        ## Put the I2C in config mode
        i2c_ref.mem_write(0b1100, 0x28, 0x3D)
        delay(190)
        
        
        
        pass
        
        
    def opMode(self, mode):
        '''!@brief     Allows the user to set the operation mode of the IMU sensor
            @details    
        '''
        ## if IMU is NOT in config mode, set to config mode
        if self.i2c.mem_read(1, 0x28, 0x3D) != b'\x00':
            self.i2c.mem_write(0b0000,0x28,0x3D)
            
        
        ## if IMU is in config mode and userinput mode = IMU, put in IMU mode, 0b1000
        elif self.i2c.mem_read(1, 0x28, 0x3D) == b'\x00' & mode == "IMU":
            self.i2c.mem_write(0b1000, 0x28, 0x3D)
            
            
        ## if IMU is in config mode and userinput mode = NDOF, put in NDOF mode, 0b1100
        if self.i2c.mem_read(1, 0x28, 0x3D) == b'\x00' & mode == "NDOF":
            self.i2c.mem_write(0b1100, 0x28, 0x3D)
            
        ## New mode requirs Delay    
        delay(190)   
        pass
    
    def getCalStatus(self):
        '''!@brief      
            @details    
        '''
        ## IMU register address 0x35 is the calibration register
        ## Set Cal_byte equal to it, then parse thru and extract calibration status
        ## NEEDS TO BE IN NDOF MODE TO READ SYSTEM STATUS
        self.i2c.mem_write(0b1100,0x28,0x3D)
        delay(190)
        print("Rotate Platform till all values reach 3:")
        while True:
            cal_byte = self.i2c.mem_read(1, 0x28, 0x35)[0]
            mag_stat = cal_byte & 0b00000011
            acc_stat = (cal_byte & 0b00001100)>>2
            gyr_stat = (cal_byte & 0b00110000)>>4
            sys_stat = (cal_byte & 0b11000000)>>6
            print(f'Mag status: {mag_stat}, Acc status: {acc_stat}, Gyr status: {gyr_stat},Sys status: {sys_stat}')
            delay(500)
            if mag_stat == 3 & acc_stat == 3 & gyr_stat ==3 & sys_stat ==3:
                break
        self.i2c.mem_read(self.cal_bytes, 0x28, 0x55)
#        
#        ##This block writes the calibration coefficients to a .txt file
        with open("IMU_cal_coeffs.txt", "w") as f:
            self.cal_string = f.write(','.join(hex(cal_byte) for cal_byte in self.cal_bytes))
#        print(self.cal_bytes)
    
        
    def getCalCoeff(self):
        '''!@brief      
            @details    
        '''
        ## NEED TO BE IN CONFIG MODE TO READ THE OFFSETS
        self.i2c.mem_write(0b0000,0x28,0x3D)
        delay(190)
        self.i2c.mem_read(self.cal_bytes, 0x28, 0x55)
        return self.cal_bytes
        

    def writeCalToIMU(self):
        '''!@brief    Reads calibration constants from the calibration file and writes them to the IMU  
            @details    
        '''
        
        f = open('IMU_cal_coeffs.txt')
        str_list = f.read()
        csv_list = str_list.split(",")
        ##Put in config mode to write calibration data
        self.i2c.mem_write(0b0000,0x28,0x3D)
        delay(190)
        for i in range(0,22):
            if i == 21:
                p = int(csv_list[i][0:4])
                addr = int(0x55)+ i
                print(hex(addr))
                print(f'Value: {p}, Address: {addr}, Hex: {hex(p)}')
                self.i2c.mem_write(p, 0x28, addr)
            else:
                p = int(csv_list[i])
                addr = int(0x55)+ i
                print(hex(addr))
                print(f'Value: {p}, Address: {addr}, Hex: {hex(p)}')
                self.i2c.mem_write(p, 0x28, addr)
        self.i2c.mem_write(0b1100,0x28,0x3D)
        delay(1000)
        print(self.i2c.mem_read(1, 0x28, 0x56))
        
        
            
            
        
        pass
    
    def readEuler(self):
        '''!@brief      
            @details    
        '''
#        self.i2c.mem_write(0b1100,0x28,0x3D)
#        delay(190)
        ## Read the 6 BYTES starting at 1A
        self.buf1 = bytearray([0,0,0,0,0,0])
        self.i2c.mem_read(self.buf1, 0x28, 0x1A)
        
        EUL_head, EUL_roll, EUL_pitch = struct.unpack('<hhh', self.buf1)
        
        #print(EUL_head/16, -EUL_roll/16, EUL_pitch/16)
        return(EUL_head/16, -EUL_roll/16, EUL_pitch/16)
        
        ## EUL_Pitch is the rotation AROUND Y
        ## EUL_ Roll is the rotation AROUND X

 
    def readAngVel(self):
        '''!@brief      
            @details    
        '''
        self.buf2 = bytearray([1,2,3,0,0,0])
        self.i2c.mem_read(self.buf2, 0x28, 0x14)
        
        GYR_x, GYR_y, GYR_z = struct.unpack('<hhh', self.buf2)
        #print(-GYR_x/16, GYR_y/16, GYR_z/16)
        return(-GYR_x/16, GYR_y/16, GYR_z/16)
        ##Rotation around X is GYR_y This is thetadotX
        ##Rotation around Y is GYR_x this is thetadotY
        
        