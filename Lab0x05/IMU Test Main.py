from pyb import I2C, delay
import struct


# i2c = I2C(1)             # create and init as a controller
i2c = I2C(1, I2C.CONTROLLER)
i2c.init(I2C.CONTROLLER, baudrate=400000)
## i2c.init(I2C.PERIPHERAL, addr=0x42)
#print(i2c.scan())
#print(i2c.is_ready(0x28))
#
#
### Put the I2C in config mode
#i2c.mem_write(0b0000, 0x28, 0x3D)
##print(i2c.mem_read(1, 0x28, 0x3D))
#
#
### IF I2C is not in config mode, place it in config mode
##if i2c.mem_read(1, 0x28, 0x3D) == b'\x00':
##        print('config')
##else:
##    print('not config')
##    i2c.mem_write(0b0000, 0x28, 0x3D)
##    
#
###Put in Accelerometer mode
#delay(1000)
#i2c.mem_write(0b0001, 0x28, 0x3D)
#
#
### Create ByteArray
#zeros = [1,1,1,1,1,1]
#buf = bytearray(zeros)
#
### Read the i2c data and write it to the bytearray
#buf = i2c.mem_read(buf, 0x28, 0x08)
#
#print(list(buf))
##595 64 8254


## Put the I2C in config mode
i2c.mem_write(0b0000, 0x28, 0x3D)
delay(190)

## Put IMU in NDOF Mode
i2c.mem_write(0b1100, 0x28, 0x3D)
delay(190)

## Create ByteArray
buf = bytearray([0,0,0,0,0,0])

## Read the i2c data and write it to the bytearray
i2c.mem_read(buf, 0x28, 0x1A)
acc_x, acc_y, acc_z = struct.unpack('>hhh', buf)
print(acc_x, acc_y, acc_z)

## Calibration
while True:
    cal_byte = i2c.mem_read(1, 0x28, 0x35)[0]
    mag_stat = cal_byte & 0b00000011
    acc_stat = (cal_byte & 0b00001100)>>2
    gyr_stat = (cal_byte & 0b00110000)>>4
    sys_stat = (cal_byte & 0b11000000)>>6
    
    print(mag_stat, acc_stat, gyr_stat, sys_stat)
    delay(200)
    if mag_stat == 3 & acc_stat  == 3 & gyr_stat == 3 & sys_stat  == 3:
        break


cal_bytes = bytearray([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
i2c.mem_read(cal_bytes, 0x28, 0x55)
print(list(cal_bytes))
    

with open("calib_val.txt", "w") as f:
    cal_string = f.write(','.join(hex(cal_byte) for cal_byte in cal_bytes))

    
    
    
    
    
    
    