
class ClosedLoop:
    
    
    def __init__(self):
        self.kp = 1
        self.kd = 1
        self.duty_min = -100
        self.duty_max = 100
        
        
    def run(self, kp, kd, ref_theta, meas_theta, ref_thetadot, meas_thetadot):
        self.kp = kp
        self.kd = kd
        self.DC = self.kp*(ref_theta - meas_theta) + self.kd*(ref_thetadot - meas_thetadot)
        ## Set the motors duty cycle to this value
        return self.DC
        
    def set_kp(self, kp_value):
        self.kp = kp_value
        
    def set_kd(self,kd_value):
        self.kd = kd_value