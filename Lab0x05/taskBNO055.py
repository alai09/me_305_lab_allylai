from time import ticks_us, ticks_add, ticks_diff
from pyb import I2C
import os
import BNO055


## CREATE OPMODE SHARE, CALL IT IN TASK USER
def taskBNO055Fcn(taskName, period, eulerangles, euler_call, ang_vel, ang_call, cal_call, coef_call, set_call):

    ## @brief The encoder object which is referenced by each task
    BNO = BNO055.BNO055(I2C(1, I2C.CONTROLLER))  
    
    ## @brief A variable representing the current state of the program
    state = 0
    
    ## @brief A timestamp, in microseconds, indicating the time when the function is first initialized.
    start_time = ticks_us()
    ## @brief A timestamp, in microseconds, indicating when the next iteration of the generator must run.
    next_time = ticks_add(start_time, period)
    
    # The finite state machine must run indefinitely.
    ## TIMER START
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
## TIMER END


            ## Check if calibration file exists        
            if state == 0:
                ##If Calib file does exist
                if "IMU_cal_coeffs.txt" in os.listdir():
                    state = 2
                    print("Has cal file")
                ##If Calib file does not exist
                else:
                    cal_call.write(True)
                    state = 1
                    print("doen't have cal file")
            ## Uncalibrated
            elif state == 1:
                print('no calib file exists')
                if cal_call.read() == True:
                    BNO.getCalStatus()
                    cal_call.write(False)
                    state = 2
                
                

            ##  opmode State                
            elif state == 2:
                ## If the user wants to change the opmode, opmode_call is true
                #if euler_call.read() == True:
                eulerangles.write(BNO.readEuler())
                
                #euler_call.write(False)
                #elif ang_call.read() == True:
                ang_vel.write(BNO.readAngVel())
                
                
                #ang_call.write(False)
                if coef_call.read() == True:
                    BNO.getCalCoeff()
                    coef_call.write(False)
                elif set_call.read() == True:
                    BNO.writeCalToIMU()
                    set_call.write(False)
                  
          
            
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")    
            yield 'Yield'
                
        else:
            yield None
            