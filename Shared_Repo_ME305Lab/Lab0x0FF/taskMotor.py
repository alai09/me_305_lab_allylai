'''!@file       taskMotor.py
    @brief      A generator function to create and interact with a motor object
    @details    The file instantiates a single motor from the Motor class. 
                The states of the function indicate whether the motor driver is
                enabled or a fault has been reported. The motor has a starting state and shifts
                immediately to a standby state.   
                
    @image      html Lab0x03STD.JPG "" width=1000
                The State Transition Diagram for the TaskUser file and UI Tasks. 
                
    @image      html Lab0x03_taskdiagram.JPG "" width=1000
                The Task Diagram for the taskUser, taskMotor and taskEncoder files. 
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb import Pin, Timer
import motor

def taskMotorFcn(taskName, period, motor_num, m_Flag, duty_cycle, duty_cycle2, w_Flag):
    '''!@brief              A generator function which updates encoder attributes at the input frequency
        @details            The task runs as a generator function and with each run updates the delta and position
                            attributes of the encoder. If the z_Flag argument is true the position of the encoder
                            is set to zero.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param motor_num    The number of the motor being controlled   
        @param m_Flag       A Share object which is used to indicate that the motor
                            duty cycle should be set to the current "duty_cycle" value
        @param duty_cycle   An input duty_cycle for the motor  
        @param duty_cycle   An input duty_cycle for motor 2  
    '''
    
    tim = Timer(3, freq = 20_000)
    ## @brief The motor object used to interface with motor 1
    mot_1 = motor.Motor(tim, Pin.cpu.B4, Pin.cpu.B5)
    ## @brief The motor object used to interface with motor 2
    mot_2 = motor.Motor(tim, Pin.cpu.B0, Pin.cpu.B1)
    
    ## @brief A variable representing the current state of the program
    state = 0

    
    ## @brief A timestamp, in microseconds, indicating when the next iteration of the generator must run.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            
            #Startup State
            if state == 0:
                state = 1
            
            # Motor waiting for input
            if state == 1:            
                if m_Flag.read() is True:    
                    if motor_num.read() == 1:
                        mot_1.set_duty(duty_cycle.read())
                    elif motor_num.read() == 2:
                        mot_2.set_duty(duty_cycle2.read())
                    m_Flag.write(False)
                elif w_Flag.read() == True:
                    mot_1.set_duty(duty_cycle.read())
                    mot_2.set_duty(duty_cycle2.read())

            # Motor Enabled
#            elif state == 2:
#                if m_Flag.read() is True:    
#                    if motor_num.read() == 1:
#                        mot_1.set_duty(duty_cycle.read())
#                    elif motor_num.read() == 2:
#                        mot_2.set_duty(duty_cycle2.read())
#                    m_Flag.write(False)
#                elif w_Flag.read() == True:
#                    mot_1.set_duty(duty_cycle.read())
#                    mot_2.set_duty(duty_cycle2.read())

            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")    
            yield 'Yield'
                
        else:
            yield None