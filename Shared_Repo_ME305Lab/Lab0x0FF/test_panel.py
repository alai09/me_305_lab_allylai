import touchpaneldrv, time 
from pyb import Pin

touchpanel = touchpaneldrv.TouchPanelDrv(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0)

i = 1
t0 = time.ticks_us()
while i < 1001:
    touchpanel.scanXYZ()
    i = i + 1
t1 = time.ticks_us()
total = t1-t0
print(total/1000)
print(touchpanel.scanXYZ())
print(type(touchpanel.scanXYZ()[0]))
print(type(touchpanel.scanXYZ()[1]))
print(type(touchpanel.scanXYZ()[2]))

# i = 1
# t2 = time.ticks_us()
# while i < 101:
#     i = i + 1
# t3 = time.ticks_us()
# added = t3-t2

# print(added/100)

# adjusted = total-added
# print("adjusted:")
# print(adjusted/1000)

# i = 1
# t0 = time.ticks_us()
# while i < 1001:
#     touchpanel.scanX()
#     i = i + 1
# t1 = time.ticks_us()
# total = t1-t0
# print(total/1000)

# i = 1
# t0 = time.ticks_us()
# while i < 1001:
#     touchpanel.scanY()
#     i = i + 1
# t1 = time.ticks_us()
# total = t1-t0
# print(total/1000)

# i = 1
# t0 = time.ticks_us()
# while i < 1001:
#     touchpanel.scanZ()
#     i = i + 1
# t1 = time.ticks_us()
# total = t1-t0
# print(total/1000)
