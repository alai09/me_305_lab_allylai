
class ClosedLoop:
    '''!@brief      
        @details    
    '''
     
    def __init__(self, kp, kd, ki):
        '''!@brief      
            @details    
        '''
        self.kp = kp
        self.kd = kd
        self.ki = ki
        self.sum = 0
        self.duty_min = -100
        self.duty_max = 100
    
    def run(self, ref, meas, ref_dot, meas_dot):
        '''!@brief      
            @details    
        '''
        if abs(meas) < 1:
            self.sum = 0
        else:
            self.sum += meas
       
        self.output = self.kp*(ref - meas) + self.kd*(ref_dot - meas_dot) - self.ki*self.sum*(1/100)
        
        ## Returns the Duty cycle/desired angle
        return self.output        
        
    def set_kp(self, kp_value):
        '''!@brief      
            @details    
        '''
        self.kp = kp_value
        
    def set_kd(self,kd_value):
        '''!@brief      
            @details    
        '''
        self.kd = kd_value
        
    def set_ki(self,ki_value):
        '''!@brief      
            @details    
        '''
        self.ki = ki_value