'''!@file       main.py
    @brief      The driving class behind implimentation of IU.
    @details    This file creates User, Encoder and Motor tasks, each that run their 
                own seperate UI's that work together to accomplish motor rotation. 
                                
    @image      html Lab0x03_taskdiagram.JPG "" width=1000
                The Task Diagram for the taskUser, taskMotor and taskEncoder files.
'''

import shares, taskUser, taskMotor, taskController, taskBNO055, taskTouchPanel

## taskUserFcn(taskName, period, motor_num, eulerangles, euler_call, ang_vel, ang_call, m_Flag, duty_cycle, fault, w_Flag, ref_vel, meas_vel, kp):
## taskBNO055Fcn(taskName, period, euler_call, ang_call):
## run(self, kp_Flag, ref_theta, des_theta, ref_thetadot, des_thetadot):
## def taskControllerFcn(taskName, period, m_Flag, eulerangles, ang_vel, kp, duty_cycle, w_Flag):
    
    
## @brief A shared variable indicating whether the encoder position should be zeroed
z_Flag = shares.Share(False)
## @brief A shared variable indicating encoder position
position = shares.Share(0)
## @brief A shared variable indicating encoder delta
delta = shares.Share(0)
## @brief A shared variable indicating the motor to interact with
motor_num = shares.Share(0)
## @brief A shared variable indicating that the motor duty cycle should be changed
m_Flag = shares.Share(False)
## @brief A shared variable indicating the duty cycle to set the given motor to
duty_cycle1 = shares.Share(0)
duty_cycle2 = shares.Share(0)
## @brief A shared variable indicating a fault condition triggered by the motor
fault = shares.Share(False)
## @brief Controller flag, indicates if the 'run' method in taskController has been called.
w_Flag = shares.Share(False)
## @brief Reference velocity, a UI value. 
ref_theta = shares.Share(0)
## @brief Measured velocity, taken from taskUser
meas_theta = shares.Share(0)
ref_thetadot = shares.Share(0)
meas_thetadot = shares.Share(0)
## @brief Porportional Gain flag, user wants to change the KP value. 
kp = shares.Share(4.5) # 
kd = shares.Share(0.20)
ki = shares.Share(35)

eulerangles = shares.Share([0,0,0,0,0,0])

euler_call = shares.Share(False)

ang_vel = shares.Share([0,0,0,0,0,0])

ang_call = shares.Share(False)

cal_call = shares.Share(False)

coef_call = shares.Share(False)
set_call = shares.Share(False)

cal_XYZ = shares.Share([0,0,0])
beta = shares.Share(False)
touch_cal = shares.Share(False)
x_pos = shares.Share(0)
y_pos = shares.Share(0)
touch_has_cal = shares.Share(False)

f_Flag = shares.Share(False)
ball_coords = shares.Share([0,0])
ball_vel = shares.Share([0,0])


if __name__ == "__main__":
    '''!@brief              Constructs an empty queue of shared values
    '''
    ## @brief A list of tasks which includes one object each of the User, Encoder, and Motor Tasks
    taskList = [taskUser.taskUserFcn      ('Task User', 10_000,  motor_num, eulerangles, euler_call, ang_vel, ang_call, cal_call, m_Flag, duty_cycle1, duty_cycle2,  w_Flag, ref_theta, meas_theta, kp, kd, coef_call, set_call, ki, cal_XYZ, beta, touch_cal, touch_has_cal, f_Flag),
                taskBNO055.taskBNO055Fcn  ('Task Encoder', 10_000, eulerangles, euler_call, ang_vel, ang_call, cal_call, coef_call, set_call),
                taskTouchPanel.taskTouchPanelFcn ('Task Touch Panel', 10_000, cal_XYZ, beta, touch_cal, x_pos, y_pos, touch_has_cal, f_Flag, ball_vel, ball_coords),
                taskController.taskControllerFcn('Task Controller', 10_000, m_Flag, eulerangles, ang_vel, kp, kd, duty_cycle1, duty_cycle2, w_Flag, ki, ball_vel, ball_coords), 
                taskMotor.taskMotorFcn    ('Task Motor', 10_000, motor_num, m_Flag, duty_cycle1, duty_cycle2, w_Flag)]
    
    while True:
            try:            
                for task in taskList:
                    next(task)    
                    
                    
            except KeyboardInterrupt:
                break
    print("Program Terminated")