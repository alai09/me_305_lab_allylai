from pyb import Pin, ADC
import micropython
from micropython import const

class TouchPanelDrv: 

    def __init__ (self, pinXp, pinXm, pinYp, pinYm): 
    
        self.ppOut = const(Pin.OUT_PP)  
        self.alg = const(Pin.ANALOG)
        # self._xScl = 176/4096
        # self._yScl = 100/4096
        
        # Xp = left; Xm = right; Yp = top; Ym = bottom; 
        # init pins with proper pin id 
        self.Xp = Pin(pinXp) 
            
        self.Xm = Pin(pinXm) 
            
        self.Yp = Pin(pinYp) 
            
        self.Ym = Pin(pinYm) 
        
    #@micropython.native    
    def scanXYZ(self):
        self.Ym.init(self.alg)
        adcYm = ADC(self.Ym)
        self.Xp.init(self.ppOut,True)        
        self.Xm.init(self.ppOut,False)         
        self.Yp.init(self.alg)    
        scan_X = adcYm.read()*176/4096-88     
        self.Yp.init(self.ppOut,False)         
        self.Xm.init(self.alg) 
        adcXm = ADC(self.Xm)
        scan_Z = adcYm.read()>0
        self.Ym.init(self.ppOut,True)
        self.Xp.init(self.alg)     
        scan_Y = adcXm.read()*100/4096-50
        return(scan_X,scan_Y,scan_Z)    
        
#                # X Scan:
#        
#        # Set ym as analog 
#        self.Ym.init(self.anlg) 
#        adcYm = ADC(self.Ym) 
#        
#        # set Xp as push pull mode with high values  
#        self.Xp.init(self.ppOut, True) 
#        # self.Xp.value(True)
#        
#        # set Xm as push pull mode with low values
#        self.Xm.init(self.ppOut, False) 
#        # self.Xm.value(False)
#        
#        # set Yp to float (input mode) 
#        self.Yp.init(self.anlg) 
#        
#        #measure voltage on Ym and convert to mm measurement based on origin
#        # 88 in mm
#        # 4008 in voltage
#        self.scan_X = adcYm.read()*self.x_scl - 88
        
        
    
    def scanX(self): 

		# Set ym as analog 
        self.Ym.init(self._alg) 
        adcYm = ADC(self.Ym) 
        
        # set Xp as push pull mode with high values  
        self.Xp.init(self._ppOut,True) 
        # self.Xp.value(True)
        
        # set Xm as push pull mode with low values
        self.Xm.init(self._ppOut,False) 
        # self.Xm.value(False)
        
        # set Yp to float (input mode) 
        self.Yp.init(self._alg) 
        
        #measure voltage on Ym and convert to mm measurement based on origin
        # to origin from axis 88 mm
        return adcYm.read()  #*self.x_scl - 88

    def scanY(self): 

		# set Xm to analog float
        self.Xm.init(self.anlg) 
        adcXm = ADC(self.Xm)
        
        # Set Yp to push pull Low
        self.Yp.init(self.ppOut,False) 
        # self.Yp.value(False)
        
        # set Xp as analog
        self.Xp.init(self.anlg) 
        
        # set Ym as push pull high
        self.Ym.init(self.ppOut,True) 
        # self.Ym.value(True)
         
        #measure voltage on Xm 
        
        return adcXm.read()  #*self.y_scl - 50

    def scanZ(self): 

		# Z Scan:
        
        # Set ym as analog 
        self.Ym.init(self.anlg) 
        adcYm = ADC(self.Ym) 
        
        # set Xp as push pull mode with high values  
        self.Xp.init(self.ppOut,True) 
        # self.Xp.value(True)
        
        # Set Yp to push pull Low
        self.Yp.init(self.ppOut,False) 
        # self.Yp.value(False)
        
        # set Xm to analog float
        self.Xm.init(self.anlg) 

        #measure voltage on Ym 
        return adcYm.read()  # > 0
    
#Scan with comments
# @micropython.native    
# def scanXYZ(self):
    
#     # X Scan:
    
#     # Set ym as analog 
#     self.Ym.init(self.anlg) 
#     adcYm = ADC(self.Ym) 
    
#     # set Xp as push pull mode with high values  
#     self.Xp.init(self.ppOut,True) 
#     # self.Xm.value(True)
    
#     # set Xm as push pull mode with low values
#     self.Xm.init(self.ppOut,False) 
    
#     # set Yp to float (input mode) 
#     self.Yp.init(self.anlg) 
    
#     self.scan_X = adcYm.read()*self.x_scl - 88
    
#     # Z Scan:
    
#     # Ym already analog
#     # Xp already push pull high
    
#     # Set Yp to push pull Low
#     self.Yp.init(self.ppOut,False) 
#     # self.Yp.value(False)
    
#     # set Xm to analog float
#     self.Xm.init(self.anlg) 
#     adcXm = ADC(self.Xm) 
    
    
#     #measure voltage on Ym 
#     self.scan_Z = adcYm.read()> 0
#     #> 0
    
#     # Y Scan:
    
#     # Xm already analog float
#     # Yp already push pull low
    
#     # set Xp as analog
#     self.Xp.init(self.anlg) 
    
#     # set Ym as push pull high
#     self.Ym.init(self.ppOut,True) 
#     # self.Ym.value(True)
     
#     #measure voltage on Xm 
#     self.scan_Y = adcXm.read()*self.y_scl - 50

#     return (self.scan_X, self.scan_Y, self.scan_Z)

