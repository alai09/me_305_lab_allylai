from time import ticks_us, ticks_add, ticks_diff
import ClosedLoop
from pyb import delay

def taskControllerFcn(taskName, period, m_Flag, eulerangles, ang_vel, kp, kd, duty_cycle1, duty_cycle2, w_Flag, ki, ball_vel, ball_coords):
    state = 0
    
##Creating ClosedLoop Object
    innerX = ClosedLoop.ClosedLoop(4.5,0.20,3) 
    innerY = ClosedLoop.ClosedLoop(4.5,0.20,35)
    outerX = ClosedLoop.ClosedLoop(1,0.1,1)
    outerY = ClosedLoop.ClosedLoop(0,0,0)
    
    ## Timing Block 
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            ## End Timing Block
            
            ## Exit initial zero state
            if state == 0:
                
                state = 1
                
           ## Disabled controller task 
            elif state == 1:
                ##User wants to run the ClosedLoop function
                if w_Flag.read() == True:
                    state = 2
                else:           
                    pass
            ## Enabled controller task        
            elif state == 2:

                if w_Flag.read() == False:
                    state = 1
                else:
                    print(ball_coords.read(), ball_vel.read())
                    print(outerX.run(0,ball_coords.read()[0],0,ball_vel.read()[0]))
                    theta = outerX.run(0,ball_coords.read()[0],0,ball_vel.read()[0])
                    print(innerX.run(theta, eulerangles.read()[1], 0, ang_vel.read()[1]))
                    #print(innerX.run(outerX.run(0,ball_coords.read()[0],0,ball_vel.read()[0]), eulerangles.read()[1], 0, ang_vel.read()[1]))
                    
                    #print(innerY.run(outerY.run(0,ball_coords.read()[1],0,ball_vel.read()[1]), eulerangles.read()[0], 0, ang_vel.read()[0]))
                #print(duty_cycle2.read())
                #m_Flag.write(True)
               
                
                
                ## User wants to change the KP value.
            
        else:
            yield None                    
                        
                        
                        
    