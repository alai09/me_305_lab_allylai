'''!@file       taskBNO055.py
    @brief      A generator function to create and interact with a BNO055 IMU object
    @details    The file instantiates a single BNO055 object for coordinate definition.
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb import I2C
import os
import BNO055


def taskBNO055Fcn(taskName, period, eulerangles, euler_call, ang_vel, ang_call, cal_call, coef_call, set_call):
    '''!@brief              
        @details 
        
        @param   taskName
        @param   Period
        @param   eulerangles
        @param   euler_call
        @param   ang_vel
        @param   ang_call
        @param   cal_call
        @param   coef_call
        @param   set_call

    '''
    
    ## @brief The encoder object which is referenced by each task
    BNO = BNO055.BNO055(I2C(1, I2C.CONTROLLER))  
    
    ## @brief A variable representing the current state of the program
    state = 0
    
    ## @brief A timestamp, in microseconds, indicating the time when the function is first initialized.
    start_time = ticks_us()
    
    ## @brief A timestamp, in microseconds, indicating when the next iteration of the generator must run.
    next_time = ticks_add(start_time, period)
    

## TIMER START
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
## TIMER END


            ## Check if calibration file exists        
            if state == 0:
                ##If Calib file does exist
                if "IMU_cal_coeffs.txt" in os.listdir():
                    print("Has cal file, writing file to IMU")
                    state = 2
                    
                ## If Calib file does not exist
                else:
                    cal_call.write(True)
                    state = 1
                    
            ## Uncalibrated, perform calibration
            elif state == 1:
                
                if cal_call.read() == True:
                    print(BNO.getCalStatus())
                    cal_call.write(False)
                    state = 2
                
                

            ## The working class for the IMU               
            elif state == 2:
                
                eulerangles.write(BNO.readEuler())
                ang_vel.write(BNO.readAngVel())
                

                if coef_call.read() == True:
                    print(BNO.getCalCoeff())
                    coef_call.write(False)
                
                elif set_call.read() == True:
                    BNO.writeCalToIMU()
                    set_call.write(False)
                
                elif cal_call.read() == True:
                    print(BNO.getCalStatus())
                    cal_call.write(False)
                    
          
            
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")    
            yield 'Yield'
                
        else:
            yield None
            