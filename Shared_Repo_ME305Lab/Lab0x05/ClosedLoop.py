'''!@file       ClosedLoop.py
    @brief      A PID control systems class. 
    @details    A class that takes in 4 arguements, two reference values, a positional value and a velocity value. This class returns an output that will 
                bring the error value down and re-test itself. Closed loop feedback controller. 
'''

class ClosedLoop:
    '''!@brief      A closed loop feedback controller used in motor movement. 
        @details    Objects of this class are given a kp, ki and kd value and are tested and retested for feedback control. 
    '''
     
    def __init__(self, kp, kd, ki):
        '''!@brief     Initializes and returns a ClosedLoop object 
            @details   Creates induvidual Closed Loop objects. Each object is a closed loop controller in itself. 
            @param kp  The proportional gain value
            @param kd  The derivitive gain value
            @param ki  The integral gain value
        '''
        
        ## @brief kp is the proportional gain value
        self.kp = kp
        
        ## @brief kd is the derivitave gain value
        self.kd = kd
        
        ## @brief ki is the integral gain value
        self.ki = ki
        
        ## @brief the sum is used in integral control
        self.sum = 0
        
    
    def run(self, ref, meas, ref_dot, meas_dot):
        '''!@brief Runs the closed loop object. Does a single test and outputs a value to be re-tested.      
            @details Runs a single iteration of the closed loop PID feedback controller.    
            @param ref   The reference positional value
            @param meas   The measured positional value
            @param ref_dot  The reference velocity value
            @param meas_dot  The measured velocity value
            @return An output for re-testing. Can be either duty cycle, position or velocity values. 
        '''
        
        ##This block is used for integral control. Resets the self.sum variable
        if abs(meas) < 0.5:
            self.sum = 0
        else:
            self.sum += meas
       
        ## @brief self.output is the output value that will be re-tested in furtehr iterations
        self.output = self.kp*(ref - meas) + self.kd*(ref_dot - meas_dot) - self.ki*self.sum*(1/100)
        
        ## Returns the Duty cycle/desired angle
        return self.output        
        
    def set_kp(self, kp_value):
        '''!@brief      Sets the proportional gain value in the middle of iterations        
        '''
        self.kp = kp_value
        pass
    
    def set_kd(self,kd_value):
        '''!@brief Sets the derivative gain value in the middle of iterations
        '''
        self.kd = kd_value
        pass
    
    def set_ki(self,ki_value):
        '''!@brief      Sets the integral gain value in the middle of iterations   
        '''
        self.ki = ki_value
        pass