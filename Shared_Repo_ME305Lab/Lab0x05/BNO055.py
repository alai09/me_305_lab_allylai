'''!@file       BNO055.py
    @brief      A class representing and interacting with a BNO055 9-DOF IMU.
    @details    This class instantiates an object to represent a BOSCH BNO055 IMU and
                provides methods to calibrate and retrieve data from from the device via I2C protocol. 
                Calibration can be performed either by calling a method to put the IMU in calibration 
                mode then manipulating it till it is calibrated or writing calibration coefficients 
                from a text file of a specific name to the IMU. This reference file name is "IMU_cal_coeffs.txt."
'''
from pyb import delay
import struct


class BNO055:
    '''!@brief      A class which represents a BOSCH BNO055 IMU      
        @details    Objects of this class contain methods to calibrate and retrieve
                    data from the IMU using the I2C object passed into the constructor. The
                    class also contains a method to set the operation mode of the IMU which can
                    be used to put the IMU in configuration, IMU, or NDOF mode.
    '''
    def __init__ (self, i2c_ref):
    
        '''!@brief  Initializes and returns a BNO055 object associated with the I2C object input 
            @details 
            @param i2c_ref this parameter is the IMU object that must be in controller mode. 
            
        '''
        ## @brief The i2c IMU object. 
        self.i2c = i2c_ref
        
        ## @brief Cal_bytes is an array that will hold the offset and calibration values. 
        self.cal_bytes = bytearray([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        
        ## Put the I2C in config mode, it must be delayed. 
        i2c_ref.mem_write(0b1100, 0x28, 0x3D)
        delay(19)
        pass
        
    def opMode(self, mode):
        '''!@brief     Allows the user to set the operation mode of the IMU sensor
            @details
            @param mode The user-specified IMU mode. 
        '''
        
        ## if IMU is NOT in config mode, set to config mode
        if self.i2c.mem_read(1, 0x28, 0x3D) != b'\x00':
            self.i2c.mem_write(0b0000,0x28,0x3D)
            
        ## if IMU is in config mode and userinput mode = IMU, put in IMU mode, 0b1000
        elif self.i2c.mem_read(1, 0x28, 0x3D) == b'\x00' & mode == "IMU":
            self.i2c.mem_write(0b1000, 0x28, 0x3D)
            
        ## if IMU is in config mode and userinput mode = NDOF, put in NDOF mode, 0b1100
        if self.i2c.mem_read(1, 0x28, 0x3D) == b'\x00' & mode == "NDOF":
            self.i2c.mem_write(0b1100, 0x28, 0x3D)
            
        ## New mode requirs Delay    
        delay(19)   
        pass
    
    def getCalStatus(self):
        '''!@brief  Returns the calibration status of the IMU on a scale from 0-3
            @details
            @return The calibration status of the IMU on a scale from 0-3
        '''
        ## IMU register address 0x35 is the calibration register
        ## Set Cal_byte equal to it, then parse thru and extract calibration status
        ## NEEDS TO BE IN NDOF MODE TO READ SYSTEM STATUS
        self.i2c.mem_write(0b1100,0x28,0x3D)
        delay(19)
        cal_byte = self.i2c.mem_read(1, 0x28, 0x35)[0]
        ## @brief the Magnetometer Status
        mag_stat = cal_byte & 0b00000011
        
        ## @brief The Accelerometer Status
        acc_stat = (cal_byte & 0b00001100)>>2
        
        ## @brief The Gyroscope Status
        gyr_stat = (cal_byte & 0b00110000)>>4
        
        ## @brief The Overall System Status
        sys_stat = (cal_byte & 0b11000000)>>6
        
        if mag_stat == 3 & acc_stat == 3 & gyr_stat ==3 & sys_stat ==3:
            return (mag_stat, acc_stat, gyr_stat, sys_stat)
        
        ## Calibration Block
        else:
            print('The system is not calibrated. Rotate Platform till all values reach 3:')
            while True:
                cal_byte = self.i2c.mem_read(1, 0x28, 0x35)[0]
                mag_stat = cal_byte & 0b00000011
                acc_stat = (cal_byte & 0b00001100)>>2
                gyr_stat = (cal_byte & 0b00110000)>>4
                sys_stat = (cal_byte & 0b11000000)>>6
                print(f'Mag status: {mag_stat}, Acc status: {acc_stat}, Gyr status: {gyr_stat},Sys status: {sys_stat}')
                delay(500)
                if mag_stat == 3 & acc_stat == 3 & gyr_stat ==3 & sys_stat ==3:
                    break
        ## End Calibration Block
                
        ## Writes the calibration coefficients to a .txt file
            print('Writing coefficients to file')
            with open("IMU_cal_coeffs.txt", "w") as f:
                self.cal_string = f.write(','.join(hex(cal_byte) for cal_byte in self.cal_bytes))
            return (mag_stat, acc_stat, gyr_stat, sys_stat)
        
    def getCalCoeff(self):
        '''!@brief      This function returns the calibration coefficients in case the user wants to see the raw hex values. 
            @details    The Calibration coefficients are in hexadecimal 
            @return     The calbration coefficients in hexadecimal format. 
        '''
        ## IMU must be in config mode to return the coefficients 
        self.i2c.mem_write(0b0000,0x28,0x3D)
        delay(19)
        self.i2c.mem_read(self.cal_bytes, 0x28, 0x55)
        return self.cal_bytes
        

    def writeCalToIMU(self):
        '''!@brief    Reads calibration constants from the calibration file and writes them to the IMU  
            @details    
        '''
        
        ## @brief the file IMU_cal_coeffs in variable format
        f = open('IMU_cal_coeffs.txt')
        
        ## @brief the list that the IMU calibration coefficients will read to
        str_list = f.read()
        
        ## @brief the comma separated list that the coefficients are stored in
        csv_list = str_list.split(",")
        
        ##Put in config mode to write calibration data
        self.i2c.mem_write(0b0000,0x28,0x3D)
        delay(19)
        
        ##IMU Writing Coefficients block
        for i in range(0,22):
            if i == 21:
                p = int(csv_list[i][0:3])
                addr = int(0x55)+ i
                self.i2c.mem_write(p, 0x28, addr)
            else:
                p = int(csv_list[i])
                addr = int(0x55)+ i
                self.i2c.mem_write(p, 0x28, addr)
        self.i2c.mem_write(0b1100,0x28,0x3D)
        delay(19)
        pass
    
    def readEuler(self):
        '''!@brief    Returns the Euler angles from the IMU unit  
            @details    
            @return The Euler angles in heading, X, then Y. 
        '''
        ## Read the 6 BYTES starting at 1A
        ## @brief the bytearray that will store the Euler angles
        self.buf1 = bytearray([0,0,0,0,0,0])
        self.i2c.mem_read(self.buf1, 0x28, 0x1A)
        EUL_head, EUL_roll, EUL_pitch = struct.unpack('<hhh', self.buf1)
        return(EUL_head/16, -EUL_roll/16, EUL_pitch/16)
        ## EUL_Pitch is the rotation AROUND Y
        ## EUL_ Roll is the rotation AROUND X

 
    def readAngVel(self):
        '''!@brief      Returns the angular velocity measurement from the gyroscope
            @details    
            @return The angular velocity measurement in X, Y, Z
        '''
        ## @brief The bytearray that holds the angular velocity data
        self.buf2 = bytearray([1,2,3,0,0,0])
        self.i2c.mem_read(self.buf2, 0x28, 0x14)
        
        GYR_x, GYR_y, GYR_z = struct.unpack('<hhh', self.buf2)
        return(-GYR_x/16, GYR_y/16, GYR_z/16)
        ##Rotation around X is GYR_y This is thetadotX
        ##Rotation around Y is GYR_x this is thetadotY
        
        