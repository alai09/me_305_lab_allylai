'''!@file       motor.py
    @brief      A class representing and interacting with a DC motor.
    @details    During each iteration, a motor object is made with its own attributes. The motor
                can set the duty cycle, or speed of the motor.
'''
from pyb import Pin, Timer

class Motor:
    '''!@brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''

    def __init__ (self, tim, IN1_pin, IN2_pin):
        '''!@brief      Initializes and returns an object associated with a DC Motor.
            @details    Objects of this class are instantiated
                        from an existing a DRV8847 object and use
                        that to create Motor objects using the method motor().
                        DRV8847.motor().
        '''
        ## @brief The first motor input pin.
        self.pin1 = IN1_pin
        
        ## @brief The second motor input pin.
        self.pin2 = IN2_pin

        # Motor 1 IF statement
        if IN1_pin == Pin.cpu.B4:
            
            ## @brief Motor timer channel 1
            self.t2c1 = tim.channel(1, Timer.PWM_INVERTED, pin = self.pin1)
            
            ## @brief Motor timer channel 2
            self.t2c2 = tim.channel(2, Timer.PWM_INVERTED, pin = self.pin2)
            
            
        # Motor 2 IF statement
        elif IN1_pin == Pin.cpu.B0:
            self.t2c1 = tim.channel(3, Timer.PWM_INVERTED, pin = self.pin1)
            self.t2c2 = tim.channel(4, Timer.PWM_INVERTED, pin = self.pin2)
        
        pass

    def set_duty (self, duty):
        '''!@brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param duty A signed number holding the duty
                        cycle of the PWM signal sent to the motor
        '''
        #FWD Direction IF statement       
        if duty > 0:
            self.t2c1.pulse_width_percent(duty)
            self.t2c2.pulse_width_percent(0)
    
        #RVS Direction IF statement
        elif duty < 0:
            self.t2c1.pulse_width_percent(0)
            self.t2c2.pulse_width_percent(-duty)
        #STOP IF statement
        elif duty == 0:
            self.t2c1.pulse_width_percent(duty)
            self.t2c2.pulse_width_percent(duty)
            
        pass


        