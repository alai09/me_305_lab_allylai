from time import ticks_us, ticks_add, ticks_diff
from pyb import Pin, USB_VCP, delay
import os
import touchpaneldrv
from ulab import numpy as np

def taskTouchPanelFcn(taskName, period, beta, touch_cal, x_pos, y_pos, touch_has_cal, f_Flag, ball_vel, ball_coords):

    
    Touchpanel = touchpaneldrv.TouchPanelDrv(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0)  
    # count = 0
    x_hat = 0
    y_hat = 0
    vx_hat = 0
    vy_hat = 0
    # alpha and beta between 0 and 1; start with alpha = 0.85 and beta = 0.005
    alpha = 0.055
    beta2 = 0.0005
    state = 0
    count = 0
    last_x_pos = 0
    last_y_pos = 0
    serport = USB_VCP()
    X = np.array([[0,0,0], [0,0,0], [0,0,0], [0,0,0], [0,0,0]])
    
    
    ## START Timing Block 
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
        
        over_T = ticks_diff(current_time,next_time)
        if over_T >= 0:
            
            ## @brief Time difference between measurements of position
            Ts = (period + over_T)/1000000
        
            next_time = ticks_add(next_time, period)
            ## END Timing Block
            if state == 0:
                state = 1
            
            if state == 1:
                print(touch_cal.read(), touch_has_cal.read())
                if ("touch_panel_cal_coeffs3.txt" in os.listdir()) & (count == 0):
                    print('here')
                    touch_has_cal.write(True)
                    print('Panel calibration file found, writing file to system') 
                    with open("touch_panel_cal_coeffs3.txt", "r") as f:
                        str_list = f.read()
                    csv_list = str_list.split(",")
                    print(csv_list)
                    myArray = np.array([  [float(csv_list[0]), float(csv_list[1])], [float(csv_list[2]), float(csv_list[3])], [float(csv_list[4]), float(csv_list[5])]  ])
                    beta.write(myArray)
                    state = 3

                elif (touch_cal.read() == True) & (touch_has_cal.read() == False):
                    print('Touch the four corners of the panel in the clockwise direction, starting with the top left. Then press the center. Press enter during each touch.')
                    state = 2
                
                elif touch_has_cal.read() == True:
                    state = 1
                elif f_Flag.read() == True:
                    state = 4              
                
                    
            if state == 2:
                Y = np.array([[-80, 40], [80, 40], [80, -40], [-80, -40], [0,0]])
                
                
                if serport.any():
                    Char = serport.read(1).decode()
                    if Char == '\r':
                        X[count] = (Touchpanel.scanXYZ()[0], Touchpanel.scanXYZ()[1],1)
                        print(X)
                        count += 1
                        
                    if count == 5:
                        beta1 = np.linalg.inv(np.dot(X.transpose(), X))
                        beta2 = np.dot(X.transpose(), Y)
                        beta.write(np.dot(beta1,beta2))
                        print('Writing coefficients to file')
                        with open("touch_panel_cal_coeffs3.txt", "w") as f:
                            f.write(    str(beta.read()[0,0]) + ',' + str(beta.read()[0,1]) + ',' + str(beta.read()[1,0]) + ',' + str(beta.read()[1,1]) + ',' + str(beta.read()[2,0]) + ',' + str(beta.read()[2,1]))                            
                       
                        
                        touch_cal.write(False)
                        touch_has_cal.write(True)
                        state = 3
                        
                        
                        
            if state == 3:
                
                 x_pos = beta.read()[0,0]*Touchpanel.scanXYZ()[0]  +  beta.read()[0,1]*Touchpanel.scanXYZ()[1]  +  beta.read()[2,0]
                 vx = (x_pos - last_x_pos)/Ts
                 y_pos = beta.read()[1,1]*Touchpanel.scanXYZ()[1]  +  beta.read()[1,0]*Touchpanel.scanXYZ()[0]  +  beta.read()[2,1]
                 vy = (y_pos - last_y_pos)/Ts
                 ball_coords.write([x_pos,y_pos,Touchpanel.scanXYZ()[2]])
                 ball_vel.write([vx,vy])
                 last_x_pos = x_pos
                 last_y_pos = y_pos
                 if Touchpanel.scanXYZ()[2] < 4000:
                     #print(vx, vy)
                     yield None
                 if f_Flag.read() == True:
                     state = 4
                
                 
                 
            if state == 4:
                 x_hat = x_hat + alpha*(Touchpanel.scanXYZ()[0] - x_hat) + Ts*vx_hat
                 y_hat = y_hat + alpha*(Touchpanel.scanXYZ()[1] - y_hat) + Ts*vy_hat
                 vx_hat = vx_hat + (beta2/Ts)*(Touchpanel.scanXYZ()[0] - x_hat)
                 vy_hat = vy_hat + (beta2/Ts)*(Touchpanel.scanXYZ()[1] - y_hat)
                 ball_coords.write([x_hat,y_hat])
                 ball_vel.write([vx_hat,vy_hat])
#                 if Touchpanel.scanXYZ()[2] < 4000:
#                     print(x_hat,y_hat)
                 yield None

            
            else:
                yield None  

                     
                        
