'''!@file       main.py
    @brief      The driving class behind implimentation of IU.
    @details    This file creates User, Encoder and Motor tasks, each that run their 
                own seperate UI's that work together to accomplish motor rotation. 
                                
    @image      html Lab0x03_taskdiagram.JPG "" width=1000
                The Task Diagram for the taskUser, taskMotor and taskEncoder files.
'''

import shares, taskUser, taskMotor, taskController, taskBNO055, taskTouchPanel
    

##Closed Loop Shares
## @brief Reference velocity, a UI value. 
ref_theta = shares.Share(0)
## @brief Measured velocity, taken from taskUser
meas_theta = shares.Share(0)
## @brief
ref_thetadot = shares.Share(0)
## @brief
meas_thetadot = shares.Share(0)
## @brief Porportional Gain flag, user wants to change the KP value. 
kp = shares.Share(4.5) # 
## @brief
kd = shares.Share(0.20)
## @brief
ki = shares.Share(35)
## @brief Controller flag, indicates if the 'run' method in taskController has been called.
w_Flag = shares.Share(False)


##Motor and Duty Cycle Shares
## @brief A shared variable indicating the motor to interact with
motor_num = shares.Share(0)
## @brief A shared variable indicating that the motor duty cycle should be changed
m_Flag = shares.Share(False)
## @brief A shared variable indicating the duty cycle to motor 1
duty_cycle1 = shares.Share(0)
## @brief A shared variable indicating the duty cycle to motor 2
duty_cycle2 = shares.Share(0)


##IMU Shares
## @brief The Euler angles returned from the IMU chip
eulerangles = shares.Share([0,0,0,0,0,0])
## @brief A flag that lets the program know that Euler angles are desired
euler_call = shares.Share(False)
## @brief The Gyroscope values returned from the IMU chip
ang_vel = shares.Share([0,0,0,0,0,0])
## @brief A flag that lets the program know that angular velocity is desired
ang_call = shares.Share(False)
## @brief A flag that lets the program know that calibration of the IMU is desired
cal_call = shares.Share(False)
## @brief A flag that lets the program know that IMU coefficients are desired
coef_call = shares.Share(False)
## @brief A flag that lets the program know that external IMU coefficients are to be written to IMU
set_call = shares.Share(False)



##TouchPanel Shares
# ## @brief A 
# cal_XYZ = shares.Share([0,0,0])
## @brief A share that holds the beta value
beta = shares.Share(False)
## @brief A flag that lets the program know that calibration is desired
touch_cal = shares.Share(False)
## @brief The X position
x_pos = shares.Share(0)
## @brief The Y position
y_pos = shares.Share(0)
## @brief A share that lets the program know that the panel is calibrated
touch_has_cal = shares.Share(False)
## @brief A share that lets the program know that alpha beta filtering is desired
f_Flag = shares.Share(False)
## @brief The ball coordinates from the touchpanel
ball_coords = shares.Share([0,0])
## @brief The balls velocity from the touchpanel
ball_vel = shares.Share([0,0])


if __name__ == "__main__":
    '''!@brief              Constructs an empty queue of shared values
    '''
    ## @brief A list of tasks which includes one object each of the User, Encoder, and Motor Tasks
    taskList = [taskUser.taskUserFcn      ('Task User', 10_000,  motor_num, eulerangles, euler_call, ang_vel, ang_call, cal_call, m_Flag, duty_cycle1, duty_cycle2,  w_Flag, ref_theta, meas_theta, kp, kd, coef_call, set_call, ki, beta, touch_cal, touch_has_cal, f_Flag),
                taskBNO055.taskBNO055Fcn  ('Task Encoder', 10_000, eulerangles, euler_call, ang_vel, ang_call, cal_call, coef_call, set_call),
                taskTouchPanel.taskTouchPanelFcn ('Task Touch Panel', 10_000, beta, touch_cal, x_pos, y_pos, touch_has_cal, f_Flag, ball_vel, ball_coords),
                taskController.taskControllerFcn('Task Controller', 10_000, m_Flag, eulerangles, ang_vel, kp, kd, duty_cycle1, duty_cycle2, w_Flag, ki, ball_vel, ball_coords), 
                taskMotor.taskMotorFcn    ('Task Motor', 10_000, motor_num, m_Flag, duty_cycle1, duty_cycle2, w_Flag)]
    
    while True:
            try:            
                for task in taskList:
                    #print(task)
                    next(task)    
                    
                    
                    
            except KeyboardInterrupt:
                break
    print("Program Terminated")