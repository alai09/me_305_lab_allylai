'''!@file       taskUser.py
    @brief      A generator to implement UI tasks as part of an FSM.
    @details    During each iteration the generator initiates actions based 
                on the entered sampling period and user inputs. The taskUser interacts
                with the taskMotor and taskEncoder classes to translate user input to
                interaction with hardware. 
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP
import array
from ulab import numpy as np

##taskUser.taskUserFcn      ('Task User', 10_000,  motor_num, eulerangles, euler_call, ang_vel, ang_call, cal_call, m_Flag, duty_cycle,  w_Flag, ref_theta, meas_theta, kp),

def taskUserFcn(taskName, period, motor_num, eulerangles, euler_call, ang_vel, ang_call, cal_call, m_Flag, duty_cycle1, duty_cycle2, w_Flag, ref_theta, meas_theta, kp, kd, coef_call, set_call, ki , beta, touch_cal, touch_has_cal, f_Flag):
    '''!@brief              A generator function which returns a value for state related to a UI task
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param z_Flag       A Share object which is used to indicate that an 
                            action related to entering the "z" key should be performed
        @param position     A Share object respresenting the position attribute of the encoder
        @param delta        A Share object respresenting the delta attribute of the encoder
    '''
    print('stuck')
    ## @brief The state that the UI is currently in.
    state = 0
    
    ## @brief A counter variable used for data collection. 
    count = 0 
    
    ## @brief Variable for time used in data collection
    time = 0.00
    
    ## @brief An array for data used in data collection
    DatArray = array.array('l',[])
    
    ## @brief An array for time data used in data collection
    timeArray = array.array('f', [])
    X_array = array.array('f', [])
    
    ## @brief A boolean used to indicate whether the duty cycle is being set or data is being collected
    collecting = False
    ## @brief A number used to keep track of the sum of all the velocities sampled to calculate average velocity
    velo_sum = 0
    ## @brief An integer used to keep track of how many velocities have been sampled to calculate average velocity
    v_count = 0
    ## @brief An array for duty cycle specified data collection
    DutyArray = array.array('l',[])
    ## @brief An array for average velocity data used in data collection
    VelocityArray = array.array('f', [])
    
 
    ## @brief The time that the UI tasks started. 
    start_time = ticks_us()
    
    ## @brief The start time plus the period indicating when the next cycle of UI begins
    next_time = ticks_add(start_time, period)
    
    ## @brief A (virtual) serial port object used for getting characters cooperatively.
    serport = USB_VCP()
    
    print('+-------------------------------------+')
    print('| Welcome to the Ball Balancing       |') 
    print('| Platforms User Interface            |')
    print('|-------------------------------------|')
    print('| To use the interface, press:        |')
    print('| "P" to print Euler angles from the  |') 
    print('|         IMU                         |')
    print('| "V" to print angular velocity from  |')
    print('|         the IMU                     |')
    print('| "U" to print calibration status     |')
    print('| "C" to write calibration coeffs     |')
    print('|         to IMU                      |')
    print('| "Q" to calibrate touch panel        |')
    print('| "F" to filter touch panel readings  |')
    print('| "m" to enter duty cycle for motor 1 |')
    print('| "M" to enter duty cycle for motor 2 |')
    print('| "T" to start testing interface      |')    
    print('| "S" to stop data collection or test |')
    print('|      interface prematurely          |')
    print('| "H" to print this help menu         |')
    print('+-------------------------------------+') 
    print('| Closed Loop Controller Functions:   |')
    print('|                                     |')
    print('| press "Y" to choose a set point for |')
    print('|           closed-loop control       |')
    print('| press "K" to enter new gain values  |')
    print('| press "W" to enable or disable      |')
    print('|           platform balancing        |')
    print('+-------------------------------------+')
      
    while True:
        ## @brief The current time.
        current_time = ticks_us()
   
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
            
            if state == 0:
          
#                state = 1  
#   
#            elif state == 1:
                
                if serport.any():
                    ## @brief The character input for UI.
                    charIn = serport.read(1).decode()
                    
                    ## User in == P
                    if charIn in {'p','P'}:
                        print('Typed P for Euler Angles')
                        euler_call.write(True)
                        print(eulerangles.read())
                        
                    if charIn in {'v','V'}:
                        print('Typed V for Angular Velocity')
                        ang_call.write(True)
                        print(ang_vel.read())                    
                        
                    if charIn in {'u','U'}:
                        print('Typed U for Calibration status')
                        cal_call.write(True)
                        
                    if charIn in {'C','c'}:
                        print('Typed C')
                        set_call.write(True)
                        
                    if charIn in {'Q','q'}:
                        touch_cal.write(True)
                        touch_has_cal.write(False)
                        
#                        if touch_has_cal.read() == True:
#                            print("Writing calibration coefficients to Touch Panel")
#                            
#                        else:    
#                            touch_cal.write(True)
                            
                            
                        
                    if charIn in {'m','M'}:
                        print(charIn)
                        if charIn == 'm':
                            motor_num.write(1)
                        else:
                            motor_num.write(2)
                        state = 5     
                        
                    if charIn in {'t','T'}:
                        state = 7
                        collecting = False
                        motor_num.write(1)
                        print("Entering Testing Interface...")
                        print(" ")
                        print('Please enter a duty cycle between -100 and 100 for Motor 1')
                        
                    if charIn in {'s','S'}:
                            print('Data is not being collected, press G to start data collection')
                            
                    if charIn in {'h','H'}:
                        print('+-------------------------------------+')
                        print('| Welcome to the Ball Balancing       |') 
                        print('| Platforms User Interface            |')
                        print('|-------------------------------------|')
                        print('| To use the interface, press:        |')
                        print('| "P" to print Euler angles from the  |') 
                        print('|         IMU                         |')
                        print('| "V" to print angular velocity from  |')
                        print('|         the IMU                     |')
                        print('| "U" to print calibration status     |')
                        print('| "C" to write calibration coeffs     |')
                        print('|         to IMU                      |')
                        print('| "m" to enter duty cycle for motor 1 |')
                        print('| "M" to enter duty cycle for motor 2 |')
                        print('| "T" to start testing interface      |')    
                        print('| "S" to stop data collection or test |')
                        print('|      interface prematurely          |')
                        print('| "H" to print this help menu         |')
                        print('+-------------------------------------+') 
                        print('| Closed Loop Controller Functions:   |')
                        print('|                                     |')
                        print('| press "Y" to choose a set point for |')
                        print('|           closed-loop control       |')
                        print('| press "K" to enter new gain values  |')
                        print('| press "W" to enable or disable      |')
                        print('|           platform balancing        |')
                        print('+-------------------------------------+')

                            
                    
                    ## CLOSED LOOP INTERFACE BEGIN
                    ## User in == Y
                    elif charIn in {'y','Y'}:
                        print('Enter a desired velocity')
                        buffer = ''
                        while True:
                            if serport.any():
                                Char = serport.read(1).decode()
                                if Char.isdigit():
                                    buffer += Char
                                if (Char == '-') and (len(buffer) == 0):
                                    buffer += Char
                                if (Char == '\b') or (Char == '\x08'):
                                    if len(buffer) != 0:
                                        buffer(-1).remove
                                if Char == '\r':
                                    buffer = str(buffer) 
                                    ref_theta.write(int(buffer))
                                    break
                                
                    elif charIn in {'k','K'}:
                        print('Enter a direct proportional gain')
                        buffer = ''
                        while True:
                            if serport.any():
                                Char = serport.read(1).decode()
                                if Char.isdigit():
                                    buffer += Char
                                if (Char == '-') and (len(buffer) == 0):
                                    buffer += Char
                                if (Char == '.') and (buffer not in {'.'}):
                                    buffer += Char
                                if (Char == '\b') or (Char == '\x08'):
                                    if len(buffer) != 0:
                                        buffer(-1).remove
                                if (Char == '\r') and (len(buffer) > 0):
                                    buffer = str(buffer) 
                                    kp.write(float(buffer))
                                    print(f'Proportional Gain set to: {buffer}')
                                    break
                        print('Enter a derivative gain')
                        buffer = ''
                        while True:
                            if serport.any():
                                Char = serport.read(1).decode()
                                if Char.isdigit():
                                    buffer += Char
                                if (Char == '-') and (len(buffer) == 0):
                                    buffer += Char
                                if (Char == '.') and (buffer not in {'.'}):
                                    buffer += Char
                                if (Char == '\b') or (Char == '\x08'):
                                    if len(buffer) != 0:
                                        buffer(-1).remove
                                if (Char == '\r') and (len(buffer) > 0):
                                    buffer = str(buffer) 
                                    kd.write(float(buffer))
                                    print(f'Derivative Gain set to: {buffer}')
                                    break
                        print('Enter an integral gain')
                        buffer = ''
                        while True:
                            if serport.any():
                                Char = serport.read(1).decode()
                                if Char.isdigit():
                                    buffer += Char
                                if (Char == '-') and (len(buffer) == 0):
                                    buffer += Char
                                if (Char == '.') and (buffer not in {'.'}):
                                    buffer += Char
                                if (Char == '\b') or (Char == '\x08'):
                                    if len(buffer) != 0:
                                        buffer(-1).remove
                                if (Char == '\r') and (len(buffer) > 0):
                                    buffer = str(buffer) 
                                    ki.write(float(buffer))
                                    print(f'Integral Gain set to: {buffer}')
                                    break
                      
                                
                    elif charIn in {'w','W'}:
                        if w_Flag.read() == True:
                            print('Closed loop OFF')
                            duty_cycle1.write(0)
                            duty_cycle2.write(0)
                            w_Flag.write(False)
                        elif w_Flag.read() == False:
                            print('Closed loop ON')
                            w_Flag.write(True)
                            
                    elif charIn in {'f','F'}:
                        if f_Flag.read() == True:
                            print('Touch Panel Filter Turned OFF')
                            f_Flag.write(False)
                        elif f_Flag.read() == False:
                            print('Touch Panel Filter Turned ON')
                            f_Flag.write(True)
                            
                            



            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield state
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None
