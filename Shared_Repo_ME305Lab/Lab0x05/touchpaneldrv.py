from pyb import Pin, ADC, delay , Timer
class TouchPanelDrv: 


    def __init__ (self, pinXp, pinXm, pinYp, pinYm): 
    
        # Xp = left; Xm = right; Yp = top; Ym = bottom; 
    
        # init pins as push-pull with proper pin id 
        self.Xp = Pin(pinXp, Pin.OUT_PP) 
            
        self.Xm = Pin(pinXm, Pin.OUT_PP) 
            
        self.Yp = Pin(pinYp, Pin.OUT_PP) 
            
        self.Ym = Pin(pinYm, Pin.OUT_PP) 
        
        #print(self.Xp.names()) 
        
    def scanXYZ(self):
        
        		# Set ym as analog 
        self.Ym.init(Pin.ANALOG) 
        adcYm = ADC(self.Ym) 
        
        		 # set Xp as high push pull 
        # self.Xp.init(Pin.OUT_PP, pull = Pin.PULL_UP) 
        self.Xp.init(Pin.OUT_PP) 
        self.Xp.value(True)
        
        		# set Xm as low push pull 
        # self.Xm.init(Pin.OUT_PP, pull = Pin.PULL_DOWN) 
        self.Xm.init(Pin.OUT_PP) 
        self.Xm.value(False)
        		# float Yp 
        self.Yp.init(Pin.IN) 
        
        # 		# settling period (5us -> from graph takes about 3.6 us to settle) 
        #         delay(5) 
        
        		#measure voltage on Ym and convert to mm measurement based on origin
        self.scanX = (adcYm.read()/4096)*176 - 88 
        
        	# Set Xm as analog 
        self.Xm.init(Pin.ANALOG) 
        adcXm = ADC(self.Xm) 
        		# set Yp as high push pull 
        self.Yp.init(Pin.OUT_PP) 
        self.Yp.value(True)
        
        		# set Ym as low push pull 
        self.Ym.init(Pin.OUT_PP) 
        self.Ym.value(False)
        
        		# float Xp 
        self.Xp.init(Pin.IN) 
        
        # 		# settling period (5us -> from graph takes about 3.6 us to settle) 
        #         delay(5) 
        
        		#measure voltage on Xm 

        self.scanY = (adcXm.read()/4096)*100 - 50
        
        		# Set Ym as analog 
        self.Ym.init(Pin.ANALOG) 
        adcYm = ADC(self.Ym) 
        
        		# set Yp as high push pull 
        self.Yp.init(Pin.OUT_PP) 
        self.Yp.value(True)
        
        		# set Xm as low push pull 
        self.Xm.init(Pin.OUT_PP) 
        self.Xm.value(False)
        
        		# float Xp 
        self.Xp.init(Pin.IN) 
        
        # 		# settling period (5us -> from graph takes about 3.6 us to settle) 
        #         delay(5) 
        
        		#measure voltage on Ym 
        self.scanZ = adcYm.read() 
        
        #return('no check')
        return (self.scanX, self.scanY, self.scanZ)

