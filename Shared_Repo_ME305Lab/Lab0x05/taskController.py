from time import ticks_us, ticks_add, ticks_diff, ticks_ms
import ClosedLoop
from pyb import delay

def taskControllerFcn(taskName, period, m_Flag, eulerangles, ang_vel, kp, kd, duty_cycle1, duty_cycle2, w_Flag, ki, ball_vel, ball_coords):
    state = 0
    
   
    ## No Contact CLC
    innerX_no_ball = ClosedLoop.ClosedLoop(0.25, 0.2, 30) 
    innerY_no_ball = ClosedLoop.ClosedLoop(0.25, 0.2, 30)
    
    ##Contact CLC
    innerX = ClosedLoop.ClosedLoop(3, 0.25, 2) 
    innerY = ClosedLoop.ClosedLoop(3, 0.25, 2)
    outerX = ClosedLoop.ClosedLoop(0.1, 0.09, 0)
    outerY = ClosedLoop.ClosedLoop(0.1, 0.09, 0)
    count = 0 
    count2 = 0
    LDC1 = 0
    LDC2 = 0
    
    
## TIMER START 
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
        
            next_time = ticks_add(next_time, period)
## TIMER END


            
            ## Exit initial zero state
            if state == 0:
                state = 1
                
           ## Disabled controller task 
            elif state == 1:
                ##User wants to run the ClosedLoop function
                if w_Flag.read() == True:
                    state = 2
                else:           
                    pass
            ## Enabled controller task        
            elif state == 2:

                if w_Flag.read() == False:
                    print('END CLC')
                    duty_cycle1.write(0)
                    duty_cycle2.write(0)
                    print(duty_cycle1.read())
                    state = 1
                else:
                    
                    theta1 = outerX.run(0,ball_coords.read()[0],0,ball_vel.read()[0])
                    DC1 = innerX.run(theta1, eulerangles.read()[2], 0, ang_vel.read()[0])
                    
                    theta2 = outerY.run(0,ball_coords.read()[1],0,ball_vel.read()[1])
                    DC2 = innerY.run(-theta2, eulerangles.read()[1], 0, ang_vel.read()[1])
                    
                    ## SATURATION BLOCKS
                    if theta1 > 12:
                        theta1 = 12
                        
                    elif theta1 < -12:
                        theta1 = -12
                        
                        
                    if theta2 > 12:
                        theta2 = 12
                        
                    elif theta2 < -12:
                        theta2 = -12
#                    print(ball_coords.read())
#                    
                    if (ball_coords.read()[2] < 4000):
                        count2 = 0
                        if count2 < 200:
                            if (DC1 > 60):
                                DC1 = 60
                                duty_cycle1.write(DC1)
                                
                            elif (DC1 < -60):
                                DC1 = -60
                                duty_cycle1.write(DC1)
                                
                            elif (DC1 < 60) & (DC1 > -60):
                                duty_cycle1.write(DC1)
                            LDC1 = DC1 
            #                        
            #                        
                            if (DC2 > 60):
                                DC2 = 60
                                duty_cycle2.write(DC2)
                                
                                
                            elif (DC2 < -60):
                                DC2 = -60
                                duty_cycle2.write(DC2)
                                
                                
                            elif (DC2 < 60) & (DC2 > -60):
                                duty_cycle2.write(DC2)
                                
                            LDC2 = DC2
                            count = 0
                            count2 += 1
                            print(DC1)
                        count += 1
#                    
#                    
#                    
#                    
#
#                    
#                    
##                    
##                    ## MOTOR 1
##
##                        count = 0
##                        pass
##                        
##                    
##                        #print(-45)
##                        duty_cycle1.write(-45)
##                        count = 0
##                        pass
##                    
##                    
##                        #print(innerX.run(theta1, eulerangles.read()[2], 0, ang_vel.read()[0]))
##                        duty_cycle1.write(innerX.run(theta1, eulerangles.read()[2], 0, ang_vel.read()[0]))
###                        print(duty_cycle1.read())
##                        count = 0
##                        pass
##                    print(theta1)
##                        
#                        
#                       ## MOTOR 2 
##                    if (DC2 > 45) & (ball_coords.read()[1] < 4000):
##                        #print(-45)
##                        duty_cycle2.write(-45)
##                        count = 0
##                        pass
##                    elif (DC2 < -45) & (ball_coords.read()[2] < 4000):
##                        #print(45)
##                        duty_cycle2.write(45)
##                        count = 0
##                        pass
##                    elif (DC2 < 45) & (DC2 > -45) & (ball_coords.read()[2] < 4000):
##                        #print(-innerX.run(theta2, eulerangles.read()[1], 0, ang_vel.read()[1]))
##                        duty_cycle2.write(-innerX.run(theta2, eulerangles.read()[1], 0, ang_vel.read()[1]))
##                        count = 0
##                        pass
#                    
                    count += 1
                    ## No Contact Block
                    if  (count > 250) & (ball_coords.read()[2] > 4000):
                        print('self-level')
                        
                        DC3 = innerX_no_ball.run(0, eulerangles.read()[2], 0, ang_vel.read()[0])
                        DC4 = innerY_no_ball.run(0, eulerangles.read()[1], 0, ang_vel.read()[1])

                        
                        ##Saturation
                        if (DC3 > 45):
                            duty_cycle1.write(45)  
                        elif (DC3 < -45):
                            duty_cycle1.write(-45)
                        else: 
                            duty_cycle1.write(DC3)
                        
                        
                        if (DC4 > 45):
                            duty_cycle2.write(45)  
                        elif (DC4 < -45):
                            duty_cycle2.write(-45)
                        else:
                            duty_cycle2.write(DC4)

                
                ## User wants to change the KP value.
            
        else:
            yield None                    
                        
                        
                        
    