'''!@file       taskEncoder.py
    @brief      A generator to update encoder attributes
    @details    The generator always updates the current position and delta of 
                the referenced encoder to the shared position and delta variables if the
                indicated time period has elapsed since the last run of the generator function.
                When flagged to do so the position of the encoder can be set to zero with the
                encoder attributes updating afterwards.
'''

from time import ticks_us, ticks_add, ticks_diff
import encoder

def taskEncoderFcn(taskName, period, z_Flag, position, delta, meas_vel):
    '''!@brief              A generator function which updates encoder attributes at the input frequency
        @details            The task runs as a generator function and with each run updates the delta and position
                            attributes of the encoder. If the z_Flag argument is true the position of the encoder
                            is set to zero.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param z_Flag       A Share object which is used to indicate that an 
                            action related to entering the "z" key should be performed
        @param position     A Share object respresenting the position attribute of the encoder
        @param delta        A Share object respresenting the delta attribute of the encoder
        @param meas_vel     A measured velocity based on the delta attribute of the encoder
    '''
    ## @brief The encoder object which is referenced by each task
    enc1 = encoder.Encoder('B',4)
    
    ## @brief An integer representing the number of counts per revolution specified for the encoder
    CPR = 4000   
    
    ## @brief A variable representing the current state of the program
    state = 0
    
    ## @brief A timestamp, in microseconds, indicating the time when the function is first initialized.
    start_time = ticks_us()
    ## @brief A timestamp, in microseconds, indicating when the next iteration of the generator must run.
    next_time = ticks_add(start_time, period)
    ## @brief A timestamp, in microseconds, indicating when the last iteration was run
    last_time = 0
    
    # The finite state machine must run indefinitely.
    while True:
        current_time = ticks_us()
    
        if ticks_diff(current_time,next_time) >= 0:
            ## @brief A number in ticks indicating the timestep of the last run of the generator
            delta_t = ticks_diff(current_time, last_time)/1_000_000 #seconds
            if state == 0:
                state = 1
                
            elif state == 1:
                if z_Flag.read() == True:
                    enc1.zero()
                    print("Encoder Position Set to Zero")
                    z_Flag.write(False)
                enc1.update()
                position.write(enc1.get_position())
                delta.write(enc1.get_delta())
                # Counts/Tick(us) converted to rad/s using CPR value of encoder
                meas_vel.write(2.0*3.14159*(float(delta.read())/(CPR*delta_t)))
            
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")    
            next_time = ticks_add(next_time, period)
            yield enc1.get_position()
            last_time = current_time        
        else:
            yield None