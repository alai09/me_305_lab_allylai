'''!@file       main.py
    @brief      The driving class behind implimentation of IU.
    @details    This file creates User, Encoder and Motor tasks, each that run their 
                own seperate UI's that work together to accomplish motor rotation. 
                                
    @image      html Lab0x03_taskdiagram.JPG "" width=1000
                The Task Diagram for the taskUser, taskMotor and taskEncoder files.
'''

import shares, taskUser, taskEncoder, taskMotor, taskController

## @brief A shared variable indicating whether the encoder position should be zeroed
z_Flag = shares.Share(False)
## @brief A shared variable indicating encoder position
position = shares.Share(0)
## @brief A shared variable indicating encoder delta
delta = shares.Share(0)
## @brief A shared variable indicating the motor to interact with
motor_num = shares.Share(0)
## @brief A shared variable indicating that the motor duty cycle should be changed
m_Flag = shares.Share(False)
## @brief A shared variable indicating the duty cycle to set the given motor to
duty_cycle = shares.Share(0)
## @brief A shared variable indicating a fault condition triggered by the motor
fault = shares.Share(False)
## @brief Controller flag, indicates if the 'run' method in taskController has been called.
w_Flag = shares.Share(False)
## @brief Reference velocity, a UI value. 
ref_vel = shares.Share(0)
## @brief Measured velocity, taken from taskUser
meas_vel = shares.Share(0)
## @brief Prpportional Gain flag, user wants to change the KP value. 
kp = shares.Share(0)


if __name__ == "__main__":
    '''!@brief              Constructs an empty queue of shared values
    '''
    ## @brief A list of tasks which includes one object each of the User, Encoder, and Motor Tasks
    taskList = [taskUser.taskUserFcn      ('Task User', 10_000,  motor_num, z_Flag, position, delta, m_Flag, duty_cycle, fault, w_Flag, ref_vel, meas_vel, kp),
                taskEncoder.taskEncoderFcn('Task Encoder', 10_000, z_Flag, position, delta, meas_vel),
                taskController.taskControllerFcn('Task Controller', 10_000, m_Flag, ref_vel, meas_vel, kp, duty_cycle, w_Flag), 
                taskMotor.taskMotorFcn    ('Task Motor', 10_000, motor_num, m_Flag, w_Flag, duty_cycle, fault)]
    
    while True:
            try:            
                for task in taskList:
                    next(task)    
            except KeyboardInterrupt:
                break
    print("Program Terminated")