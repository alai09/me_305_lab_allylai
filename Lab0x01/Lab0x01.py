'''!@file                    Lab_0x01.py
    @brief                   This file provides a package that changes the blinking patterns on an LED.
    @details                 <iframe width="560" height="315" src="https://www.youtube.com/embed/lUn68gv998k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    @author                  Ally Lai
    @author                  Jason Hu
    @copyright               This file has no license
    
    @date                    January 19, 2022
    
    @mainpage My Personal Index Page
    
    section intro_sec Introduction
 
    This is the introduction.
  
    section install_sec Installation
 
   \subsection step1 Step 1: Opening the box
 
   etc...

'''

# Importation of the libraries necessary to allow this code to run
import pyb
import time
import math


def onButtonPressFCN(IRQ_src):    
    '''!
    @brief          Detects the pressing of the user button.
  
    @details        Upon sensing the pressing of the button, the function will set the global variable 'buttonPressed' to true.
                    

    @param IRQ_src  An interrupt parameter that is true when the button is pressed.
  
    '''
    
    global buttonPressed
    buttonPressed = True

        

def SquareWave(t):
    
    '''!
    @brief          Outputs a pulse width percentage corresponding to the value of a 1Hz square wave at the inputted time.
  
    @details        The Squarewave function uses the modulus operator to take the decimal value of the input parameter time "t", then 
                    determines if that remainder is higher or lower than 0.5. When the remainder is lower, the pulse width is set to 100% and
                    when higher, the pulse width is set to 0% which appears as the light turning on and off respectively.
    

    @param t        represents a specific time point during an interval of 0-10 seconds relating to relative time in a 10 second cycle when the function is called
                    
    @return         The desired instantaneous pulse width, ranging from 0% to 100% at the inputted time in the period of a  Squarewave 
                    function. If the remainder is lower than 0.5, the function will return 0, if it is higher than 0.5, the function will
                    return 100.  
    '''
    
    remainder = t%1
    if remainder <.50:
        return 100
    else:
        return 0

def SineWave(t):
    
    '''!
    @brief          Outputs the sine wave LED blinking pattern.
  
    @details        When passed in the variable t (time), the Sinewave function uses mathematical operations to return an LED intensity
                    pattern that has an amplitude of 0.5 and is shifted up 0.5 on the Y-axis. 
    
    @param t        represents elapsed time, more specifically, the difference in time between an arbitrary measurement and whenever
                    the function is called. 
                    
    @return         The instantaneous LED intensity, ranging from 0 (fully off) to 100 (fully on) that is calculated from the Sinewave 
                    function. 
    '''
    
    return 100*(0.5+0.5*math.sin(2*3.1415*t/10))

def SawWave(t):
    
    '''!
    @brief          Outputs the square wave LED blinking pattern.
  
    @details        When passed in the variable t (time), the Squarewave function uses the modulus operator to take and return the 
                    remainder value of the time, t, multiplied by 100.
    

    @param t        represents elapsed time, more specifically, the difference in time between an arbitrary measurement and whenever
                    the function is called. 
                    
    @return         The instantaneous LED intensity, ranging from 0 (fully off) to 100 (fully on) that is calculated from the Sinewave 
                    function.  
    '''
    
    return 100*(t%1)


if __name__ == '__main__':
    
    
    ## @brief pinA5 is the pin that is connected to the LED light 
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
    
    ## @brief pinC13 is the pin that is connected to the user button 
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    
    ## @brief PWM timer used with pon A5, used to control the brightness of the LED
    tim2 = pyb.Timer(2, freq = 20000)
    
    ## @brief PWM signal used to control the brightness of the LED light attached to pin A5
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    ## @brief Button interrupt function used to determine if the button has been pressed or not
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    
    ## @brief buttonPressed is a global variable that is accessible across the entire script
    #  @details buttonPressed is true when the user button is pressed, then after switching blinking patterns, the variable buttonPressed returns to its default value of false. 
    buttonPressed = False
    
    ## @brief refers to the current state of the A5 pin LED blinking pattern is currently selected. 0 is the initial state where no
    #         pattern is selected, 1 is the squarewave pattern, 2 is the sinewave pattern and 3 is the sawtooth pattern.
    #  @details The state variable is used to determine which state the user is in, and increased by 1 every time the button is pressed. This
    #         allows the program to switch from one state to another. The variable state will start at 0, then move from 1-3, then from 1-3 again
    #         and will not return to 0 unless the program is interrupted and restarted. 0 is the initial state where no
    #         pattern is selected, 1 is the squarewave pattern, 2 is the sinewave pattern and 3 is the sawtooth pattern.
    state = 0
    
    ## @brief start is the variable that holds the arbitrary starting time. It is later used to calculate a temporal difference.
    #  @details start is set to equal an arbitrary number, lets say 7000. Later when the variable s_time is calcualted, the difference between
    #           start and a value of time is calcualted. lets say that when this is calculated, the function time.ticks.ms returns a value of 7007
    #           this means that the variable s_time is equal to 7. The variable start is reset every time the button is pressed, allowing for
    #           calculation of elapsed time since the last button press
    start = time.ticks_ms()
    ## @brief  s_time is the difference between the arbitrary time and the current time. It represents exactly how much time has elapsed.
    #  @details the variable s_time is a measurement of the elapsed time that has passed by since the initial run of the program. The 
    #  variable s_time also resets itself to 0 once 10 seconds has passed. 
    s_time = start
    print('Welcome to the LED pattern program, press the user button (B1) to begin cycling')
    
    while True:
        try:
            
            ## @brief s_time is the difference between the arbitrary time and the current time. It represents exactly how much time has elapsed.
            #  @details the variable s_time is a measurement of the elapsed time that has passed by since the initial run of the program. The 
            #           variable s_time also resets itself to 0 once 10 seconds has passed. 
            #
            # s_time = (time.ticks_ms()-start)/1000
            #if s_time > 10:
             #   start = time.ticks_ms()
           
            
            if state == 0:
                
                if buttonPressed:
                    buttonPressed = False
                    print("Squarewave pattern selected")
                    state = 1
                    start = time.ticks_ms()
                    
            elif state == 1:
                
                s_time = ((time.ticks_ms()-start)/1000)%10
                ## @brief a variable representing the pulse width percentage 
                #  @details brt represents LED brightness and is determined through any of the three wave functions, this variable is continueously
                #           updated as the variable t (time) passes by               
                brt = SquareWave(s_time)
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    print("Sinewave pattern selected")
                    state = 2
                    start = time.ticks_ms()

            elif state == 2:
                s_time = ((time.ticks_ms()-start)/1000)%10
                brt = SineWave(s_time)
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    print("Sawwave pattern selected")
                    state = 3
                    start = time.ticks_ms()
         
            elif state == 3:
                s_time = ((time.ticks_ms()-start)/1000)%10
                brt = SawWave(s_time)
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    print("Squarewave pattern selected")
                    state = 1
                    start = time.ticks_ms()
     
        except KeyboardInterrupt:
            break
    print("Program Terminated")
                
